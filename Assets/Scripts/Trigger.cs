﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class Trigger : MonoBehaviour 
{


	public string	        	RemoteIP = "192.168.31.240";
	public string	        	RemoteIP2 = "192.168.31.241";

	public static int 			SendToPort = 9000; 
	public static int 			ListenerPort = 8000; // Listener x-OSC
	public static Osc 			handler;
	public static Osc           handler2;
	public static Osc           handlerLeftBattery;
	public static Osc           handlerRightBattery;
	public static UDPPacketIO 	udp;
	public static UDPPacketIO   udp2;
	public static string 		address;



	//UI STUFF
	public Text LeftGloveBatteryText;
	public Text RightGloveBatteryText;
	public Text LeftGloveTempText;
	public Text RightGloveTempText;
	public GameObject PanelGui;
	private bool show = false;


	private Vector3 xOscAcc = new Vector3(0.0f,0.0f,0.0f);
	private Vector3 xOscAcc2 = new Vector3(0.0f,0.0f,0.0f);

	//questi tengono traccia della storia dell'accelerazione: se aumenti la history sz, servira' un colpo piu' violento per sparare il pugno
	private const int xOscHistSz = 10;
	private int xOscHistId = 0;
	private float[] xOscAccMagHist;
	private float avgAcc = 1.0f;
	private float dAccMag = 0.0f;
	private const int xOscHistSz2 = 10;
	private int xOscHistId2 = 0;
	private float[] xOscAccMagHist2;
	private float avgAcc2 = 1.0f;
	private  float dAccMag2 = 0.0f;

	private float tempLeft = 0;
	private float tempRight = 0;
	private float batteryLeft = 0;
	private float batteryRight = 0;

	//questa e' la soglia per il pugno, espressa in termini di delta magnitudine dell'accelerazione
	//ignora la direzione di G e del pugno, quindi spari sempre, senza preoccuparti di tenere traccia dell'IMU
	//valore piu' basso = maggiore sensibilita'

	//private GameObject pointCloudActive;
	public static bool startGame;
	public static bool triggerLeft, triggerRight;

	public GameObject psK;
	public GameObject loopGloves;

	void Awake()
	{
		udp = gameObject.AddComponent<UDPPacketIO>() as UDPPacketIO;
		udp2 = gameObject.AddComponent<UDPPacketIO>() as UDPPacketIO;
		handler = gameObject.AddComponent<Osc>() as Osc;
		handler2 = gameObject.AddComponent<Osc> () as Osc;


		handlerLeftBattery = gameObject.AddComponent<Osc> () as Osc;
		handlerRightBattery = gameObject.AddComponent<Osc> () as Osc;

		//pointCloudActive = GameObject.Find ("KINECT");

		triggerLeft = false;
		triggerRight = false;
	
	}

	// Use this for initialization
	void Start () 
	{
		startGame = false;

		PanelGui.SetActive (false);

		setupOSC ();
		setupAcceleration ();
	}
		
	void setupAcceleration(){
		xOscAccMagHist = new float[xOscHistSz];
		xOscAccMagHist2 = new float[xOscHistSz2];
		for (int i=0; i<xOscAccMagHist.Length; i++) {
			xOscAccMagHist[i] = 1.0f;
			xOscAccMagHist2 [i] = 1.0f;
		}
		xOscHistId = 0;
		xOscHistId2 = 0;
	}

	public void setupOSC()
	{
		udp.init(RemoteIP, SendToPort, ListenerPort);

		handler.init(udp);
		handler.SetAddressHandler("/guantoni/imu", onOSC);

		handlerRightBattery.init (udp);
		handlerRightBattery.SetAddressHandler ("/guantoni/battery", onOSC);

		udp2.init (RemoteIP2, SendToPort, ListenerPort);

		handler2.init (udp2);
		handler2.SetAddressHandler ("/guantonisx/imu", onOSC);

		handlerLeftBattery.init (udp2);
		handlerLeftBattery.SetAddressHandler ("/guantonisx/battery", onOSC);
	}

	public void onOSC(OscMessage oscMessage){

		address = oscMessage.Address;

		if (address == "/guantoni/imu") 
		{
			xOscAcc.x = float.Parse (oscMessage.Values [3].ToString ());	
			xOscAcc.y = float.Parse (oscMessage.Values [4].ToString ());
			xOscAcc.z = float.Parse (oscMessage.Values [5].ToString ());

			float mag = xOscAcc.magnitude;
			//UnityEngine.Debug.LogError("ACC "+mag);
			dAccMag = Mathf.Abs (mag - avgAcc);
				
			//UnityEngine.Debug.LogError("AVG: "+avgAcc+" / NOW: "+mag);

			xOscAccMagHist [xOscHistId] = mag;
			xOscHistId++;
			if (xOscHistId >= xOscHistSz) {
				xOscHistId = 0;
			}

			avgAcc = xOscAccMagHist [0];
			for (int i = 1; i < xOscAccMagHist.Length; i++) {
				avgAcc += xOscAccMagHist [i];
			}
			avgAcc /= xOscAccMagHist.Length;
			tempRight = (float)oscMessage.Values [9];
				

		}
		else if (address == "/guantonisx/imu") 
		{

			xOscAcc2.x = float.Parse (oscMessage.Values [3].ToString ());	
			xOscAcc2.y = float.Parse (oscMessage.Values [4].ToString ());
			xOscAcc2.z = float.Parse (oscMessage.Values [5].ToString ());

			float mag2 = xOscAcc2.magnitude;
			//UnityEngine.Debug.LogError("ACC "+mag);
			dAccMag2 = Mathf.Abs (mag2 - avgAcc2);

			xOscAccMagHist2 [xOscHistId2] = mag2;
			xOscHistId2++;
			if (xOscHistId2 >= xOscHistSz2) {
				xOscHistId2 = 0;
			}

			avgAcc2 = xOscAccMagHist2 [0];
			for (int i = 1; i < xOscAccMagHist2.Length; i++) {
				avgAcc2 += xOscAccMagHist2 [i];
			}
			avgAcc2 /= xOscAccMagHist2.Length;

			tempLeft = (float)oscMessage.Values [9];
		} 
		else if (address == "/guantonisx/battery")
		{
			batteryLeft = (float)oscMessage.Values [0];
		}
		else if (address == "/guantoni/battery") 
		{

			batteryRight = (float)oscMessage.Values [0];
		}
	}
		
	void Update () 
	{
		StatusTrigger ();

		updateGui ();

		showGui ();
	}

	public void StatusTrigger()
	{
		//if (dAccMag2 > 1.5f && dAccMag > 1.5f && !psK.activeSelf && loopGloves.activeSelf) 
		if (dAccMag2 > 1.5f && dAccMag > 1.5f && loopGloves.activeSelf) 
		{
			//Debug.LogError ("Start by xOSC");
			startGame = true;
		} 

		else if (dAccMag2 > 1.5f && psK.activeSelf)
		{
			//Debug.LogError ("Trigger LEFT");
			triggerLeft = true;
			PointCloud.triggerSin = false;
			PointCloud.timerSin = 0;
			PointCloud.startSin = PointCloud.heightScaleSin;
		} 

		else if (dAccMag > 1.5f && psK.activeSelf)
		{
			//Debug.LogError ("Trigger RIGHT");
			triggerRight = true;
			PointCloud.triggerSin = false;
			PointCloud.timerSin = 0;
			PointCloud.startSin = PointCloud.heightScaleSin;
		}
	}

	public void updateGui()
	{
		LeftGloveBatteryText.text = batteryLeft + " V";
		RightGloveBatteryText.text = batteryRight + " V";

		LeftGloveTempText.text = tempLeft + " °C";
		RightGloveTempText.text = tempRight + " °C";
	}

	public void showGui()
	{
		if (Input.GetKeyDown (KeyCode.I)) {
			show = !show;
			PanelGui.SetActive (show);
		}	
	}



}
