﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Preloading : MonoBehaviour 
{
	public Image preloading;
	public  int counter;

	void Start () 
	{
		counter = (int)preloading.rectTransform.rect.width;
	}
	
	void Update () 
	{
		counter += 1;
		preloading.rectTransform.sizeDelta = new Vector2(counter, 10);
	}
}
