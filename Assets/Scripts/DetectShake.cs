﻿using UnityEngine;
using System.Collections;

public class DetectShake : MonoBehaviour 
{
	private float shakeAmount = .3f;
	private float timerShake; 
	private bool ShakeOn;
	private float durationShake = 0.5f;

	void Start()
	{
		timerShake = 0.0f;
		ShakeOn = false;
	}

	void OnCollisionEnter(Collision collider) 
	{
		if (collider.gameObject.tag == "CENTER")
		{
			//Debug.LogError ("SHAKE: " + collider.gameObject.name);

			ShakeOn = true;
			timerShake = 0.0f;

			DestroyCollider.cnt -= 1;

			this.gameObject.GetComponent<AudioSource> ().Play ();
			this.gameObject.GetComponent<AudioSource> ().loop = false;

			DMXManager.actualMode = DMXManager.SetDmxMode.FastRnd;

			Destroy (collider.gameObject);

		}
			
	}
		
	void Update()
	{
		if (ShakeOn) 
		{
			timerShake += Time.deltaTime;

			Camera.main.transform.localPosition = Random.insideUnitSphere * shakeAmount; 

			if (timerShake >= durationShake) 
			{
				ShakeOn = false;

				DMXManager.actualMode = DMXManager.SetDmxMode.OffAndSetRedSoundMedusa;
			}



		}

	}
}
