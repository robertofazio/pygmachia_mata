﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour 
{
	public static AudioSource PygmachiaMainAudioSource;
	public static AudioSource PygmachiaSoundFXAudioSource;
	// Sound Background
	public static AudioClip soundBackgroundIdle;
	public static AudioClip soundBackgroundKinect;
	public static AudioClip soundBackgroundMedusa;

	// Sound FX
	public static AudioClip SOUNDgo;
	public static AudioClip soundStartGo;
	public static AudioClip soundCountDown;
	// Sound Colpo Pugni
	public static AudioClip movimentoPugno;
	public static AudioClip colpoPugno;

	public static float BackgroundVolume = 1.0f;

	public static float  _timer;
	private GameManager GM;
	public static bool isPlayingKinectScene = true;


	void Awake()
	{
		PygmachiaMainAudioSource = GetComponent<AudioSource> ();
		PygmachiaSoundFXAudioSource = GameObject.Find("SoundFX").GetComponentInChildren<AudioSource> ();
		PygmachiaSoundFXAudioSource.volume = 1.0f;

		//echo = GameObject.Find ("SoundFX").GetComponent<AudioEchoFilter> () as AudioEchoFilter;
		//echo.enabled = false;



	}

	void Start () 
	{
		LoadPlayAudioBackground ("Audio/Mata-SoundTrack/MaTa-SoundTrack-BackGround-0", true);

		GM = GameObject.Find ("GAME MANAGER").GetComponent<GameManager>();
		isPlayingKinectScene = true; // mettere a true

	}
	
	void Update () 
	{
		PygmachiaMainAudioSource.volume = Mathf.Lerp (PygmachiaMainAudioSource.volume, BackgroundVolume, Time.deltaTime * 0.4f);

		if(isPlayingKinectScene)
			AudioDurationKinectScene (38.5f);

		if (PygmachiaMainAudioSource.clip.name == "MaTa-SoundTrack-BackGround-4" && PygmachiaMainAudioSource.isPlaying == false) 
		{
			GM.GetComponent<EndPerformance> ().enabled = true;

		}



	}
		
	public static void LoadPlayAudioBackground(string _ResourceLoadAudioClip, bool _loop)
	{
		soundBackgroundIdle = Resources.Load(_ResourceLoadAudioClip) as AudioClip;
		PygmachiaMainAudioSource.clip = soundBackgroundIdle;

		PygmachiaMainAudioSource.loop = _loop;

		if(!PygmachiaMainAudioSource.isPlaying)
			PygmachiaMainAudioSource.Play ();
	
	}


	public static void LoadPlayAudioEffects(string _ResourceLoadAudioClip, bool _loop)
	{
		SOUNDgo = Resources.Load (_ResourceLoadAudioClip) as AudioClip;
		PygmachiaSoundFXAudioSource.clip = SOUNDgo;

		PygmachiaSoundFXAudioSource.loop = _loop;

		if (!PygmachiaSoundFXAudioSource.isPlaying)
			PygmachiaSoundFXAudioSource.Play ();
	}


	public void AudioDurationKinectScene(float _durationPointCloudScene)
	{
		if (PygmachiaMainAudioSource.clip.name == "MaTa-SoundTrack-BackGround-3") 
		{
			//Debug.LogError (PygmachiaMainAudioSource.clip.name);

			_timer += Time.deltaTime;
			float t = ((int)_timer);
			float remain = _durationPointCloudScene - t;

			if (t >= _durationPointCloudScene && t <= _durationPointCloudScene +1f) 
			{
				_timer = 0f;

				GM.PygmachiaMode (GameManager.Statement.FadeOutKinectScene);

				isPlayingKinectScene = false;

				//Debug.LogError("End Timer after: "+_durationPointCloudScene  + " , isPlayingKinectScene: "+isPlayingKinectScene);
			}

		}
	}

	public static void LoadPlayAudioColpo(string _ResourceLoadAudioClip, bool _loop, bool _echo)
	{
		colpoPugno = Resources.Load (_ResourceLoadAudioClip) as AudioClip;
		PygmachiaSoundFXAudioSource.clip = colpoPugno;
		PygmachiaSoundFXAudioSource.loop = _loop;


		PygmachiaSoundFXAudioSource.Play ();

		//echo.enabled = _echo;


		
	}

	public static void LoadPlayAudioMedusa(string _ResourceLoadAudioClip, bool _loop, float vol)
	{
		soundBackgroundMedusa = Resources.Load(_ResourceLoadAudioClip) as AudioClip;
		PygmachiaMainAudioSource.clip = soundBackgroundMedusa;

		PygmachiaMainAudioSource.loop = _loop;

		if(!PygmachiaMainAudioSource.isPlaying)
			PygmachiaMainAudioSource.Play ();

	}





}
