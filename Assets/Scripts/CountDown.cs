﻿using UnityEngine;
using System.Collections;

public class CountDown : MonoBehaviour 
{
	
	public GameObject UpOne;
	public GameObject DownOne;
	public Transform DesirePos_UpOne;
	public Transform DesirePos_DownOne;

	public GameObject UpTwo;
	public GameObject DownTwo;
	public Transform DesirePos_UpTwo;
	public Transform DesirePos_DownTwo;

	public float speed = 0.05f;

	public enum Contatore { FadeIn, Quattro, Tre, Due, Uno, FadeOut, Cloud, Destroy };
	public static Contatore contatore;

	private Color32[] redCols;
	private Texture2D redTex;

	float p;
	public float timer;
	public float timerFade;

	public PointCloud pc;

	void Start () 
	{
		p = 0;
		timer = 0;

		DesirePos_UpOne.localPosition = UpOne.transform.localPosition;
		DesirePos_DownOne.localPosition = DownOne.transform.localPosition;

		DesirePos_UpTwo.localPosition = UpTwo.transform.localPosition;
		DesirePos_DownTwo.localPosition = DownTwo.transform.localPosition;

		UpOne.GetComponent<Renderer>().material.color = new Color(1,0,0, p);
		UpTwo.GetComponent<Renderer>().material.color = new Color(1,0,0, p);
		DownOne.GetComponent<Renderer>().material.color = new Color(1,0,0, p);
		DownTwo.GetComponent<Renderer>().material.color = new Color(1,0,0, p);

	}
	void FixedUpdate()
	{
		AutomaticCount();
	}

	void Update ()
	{
		UpOne.transform.localPosition = Vector3.Lerp(UpOne.transform.localPosition, DesirePos_UpOne.transform.localPosition, speed);
		DownOne.transform.localPosition = Vector3.Lerp(DownOne.transform.localPosition, DesirePos_DownOne.transform.localPosition, speed);

		UpTwo.transform.localPosition = Vector3.Lerp(UpTwo.transform.localPosition, DesirePos_UpTwo.transform.localPosition, speed);
		DownTwo.transform.localPosition = Vector3.Lerp(DownTwo.transform.localPosition, DesirePos_DownTwo.transform.localPosition, speed);

		//StartCountDown(contatore);

	}

	Contatore StartCountDown (Contatore contatore)
	{
		switch(contatore) 
		{

			case Contatore.FadeIn:
				//print("Pulse");
				Pulse();
				break;

			case Contatore.Quattro:
				//print("quattro");
				break;

			case Contatore.Tre:
				//print("tre");
				DesirePos_UpOne.transform.localPosition = new Vector3(DesirePos_UpOne.transform.localPosition.x , 0.0f ,DesirePos_UpOne.transform.localPosition.z);
				DesirePos_DownOne.transform.localPosition = new Vector3(DesirePos_DownOne.transform.localPosition.x , 0.0f , DesirePos_DownOne.transform.localPosition.z);
			
				// ############ AUDIO SOUNDgo ############ 
				AudioManager.LoadPlayAudioEffects ("Audio/CountDown/SOUNDcountdown", false);
				break;

			case Contatore.Due:
				//print("due");
				DesirePos_UpTwo.transform.localPosition = new Vector3(DesirePos_UpTwo.transform.localPosition.x , 100.0f ,DesirePos_UpTwo.transform.localPosition.z);
				DesirePos_DownTwo.transform.localPosition = new Vector3(DesirePos_DownTwo.transform.localPosition.x , -100.0f , DesirePos_DownTwo.transform.localPosition.z);
				break;

			case Contatore.Uno:
				//print("uno");
				DesirePos_UpTwo.transform.localPosition = new Vector3(DesirePos_UpTwo.transform.localPosition.x , 0.0f ,DesirePos_UpTwo.transform.localPosition.z);
				DesirePos_DownTwo.transform.localPosition = new Vector3(DesirePos_DownTwo.transform.localPosition.x , 0.0f , DesirePos_DownTwo.transform.localPosition.z);

				break;
			case Contatore.FadeOut:
				float yScale;
				timerFade += Time.deltaTime;
				yScale = UpOne.transform.localScale.y - UpOne.transform.localScale.y * (timerFade / 2);
				UpOne.transform.localScale = new Vector3 (UpOne.transform.localScale.x, yScale, UpOne.transform.localScale.z);
				UpTwo.transform.localScale  = new Vector3 (UpTwo.transform.localScale.x, yScale, UpTwo.transform.localScale.z);
				DownOne.transform.localScale  = new Vector3 (DownOne.transform.localScale.x, yScale, DownOne.transform.localScale.z);
				DownTwo.transform.localScale  = new Vector3 (DownTwo.transform.localScale.x, yScale, DownTwo.transform.localScale.z);
					break;

			case Contatore.Cloud:
				
				Camera.main.farClipPlane = 3000;

				GameManager.kinectStuffActive = true;

				print ("Cloud");
				pc.GetComponent<PointCloud> ().psKinect.gameObject.SetActive (true);
				pc.GetComponent<PointCloud> ().bNoise = true;
				pc.GetComponent<PointCloud> ()._startTime = Time.time;
				pc.GetComponent<PointCloud> ().explodeImplodeState = PointCloud.ExplodeImplodeState.Exploding;
				pc.GetComponent<PointCloud> ()._climaxNoise = true;
				pc.GetComponent<PointCloud> ()._durationLerp = 0.8f;
				pc.GetComponent<PointCloud> ().collapseTime = pc.GetComponent<PointCloud> ()._durationLerp * 2;
				pc.GetComponent<PointCloud> ().toggleCollapse ();


					// ############ AUDIO SOUND KINECT ############ 
				//AudioManager.BackgroundVolume = 1.0f;
				AudioManager.LoadPlayAudioBackground ("Audio/Mata-SoundTrack/MaTa-SoundTrack-BackGround-3", false);
				AudioManager.PygmachiaMainAudioSource.volume = 0.4f;
				AudioManager.BackgroundVolume = 0.4f;
				// DMX 
				DMXManager.actualMode = DMXManager.SetDmxMode.KinectLeds;

				Camera.main.GetComponent<MovCamera> ().enabled = true;
				//MovCamera.enableMovment = true;

				//GameObject.Find ("HoldOn").SetActive (false);

				this.gameObject.SetActive (false);
				break;

			case Contatore.Destroy:
				print ("Destroy CountDown Script");
				break;

			default:
				//print("Zero");
				break;

		}

		return contatore;
	}


	void AutomaticCount()
	{
		timer += Time.deltaTime;

		if (timer >= 0f && timer < 4f)
			StartCountDown (Contatore.FadeIn);
		else if (timer >= 4f && timer < 4.05f)
			StartCountDown (Contatore.Quattro);
		else if (timer >= 5.5f && timer < 5.55f)
			StartCountDown (Contatore.Tre);
		else if (timer >= 7f && timer < 7.05f)
			StartCountDown (Contatore.Due);
		else if (timer >= 8.5f && timer < 8.55f)
			StartCountDown (Contatore.Uno);
		else if (timer >= 10.5f && timer < 12.5f)
			StartCountDown (Contatore.FadeOut);
		else if (timer >= 12.5)
			StartCountDown (Contatore.Cloud);
		
	}

	void Pulse()
	{
		if (p < 1)
			p += Time.deltaTime/4;
		UpOne.GetComponent<Renderer>().material.color = new Color(1,0,0, p);
		UpTwo.GetComponent<Renderer>().material.color = new Color(1,0,0, p);
		DownOne.GetComponent<Renderer>().material.color = new Color(1,0,0, p);
		DownTwo.GetComponent<Renderer>().material.color = new Color(1,0,0, p);

	}


}
