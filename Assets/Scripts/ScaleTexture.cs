﻿using UnityEngine;
using System.Collections;

public class ScaleTexture : MonoBehaviour 
{
	public Material Soffitto;
	public Material Pavimento;
	public float speed = 1;
	public static float scale;


	void Start () 
	{
		
	}
	void Update () 
	{
		
		scale = Time.time * speed;

		Soffitto.mainTextureOffset = new Vector3(1, scale);
		Pavimento.mainTextureOffset = new Vector3 (1, scale);
	}
}
