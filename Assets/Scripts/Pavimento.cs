﻿using UnityEngine;
using System.Collections;

public class Pavimento : MonoBehaviour {

	public Vector3 desireScale;
	public static float speed = 0.2f;
	public static bool _pavimento = false;

	void Start () 
	{
		desireScale = new Vector3 (10, 0, 0);
		this.transform.localScale = desireScale;
	}

	void Update () 
	{
		if (_pavimento)
			desireScale = new Vector3 (10, 0, 3);
		else if(!_pavimento)
			desireScale = new Vector3 (10, 0, 0);

		this.transform.localScale = Vector3.Lerp (this.transform.localScale, desireScale, Time.deltaTime * speed);
	}
}
