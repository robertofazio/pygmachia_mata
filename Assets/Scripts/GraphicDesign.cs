﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GraphicDesign : MonoBehaviour 
{
	// LOOP GLOVES UI
	public GameObject panelGloves;
	public Image leftHand;
	public Image rightHand;
	public Image leftGlove;
	public Image rightGlove;
	public Image ForwardLeft;
	public Image ForwardRight;
	private bool _existLoopGloves = true;
	// PULSE GLOVE START PERFORMANCE
	public RawImage startPerformaceButton;
	float pulseTimer;
	private bool changePulse = false;
	private bool _existButtonStart = true;
	// NUMBER COUNTDOWN
	public Text numCounter;
	float _timer;
	public float _duration = 4;
	// LOADING
	private Texture2D[] loading; 
	private float fCounter;
	private int lastFrame;
	private int curFrame;
	private bool _activeLoading = true;
	// FADE TO BLACK LOADING
	private float _alphaToZero = 1.0f;
	public Text holdOn;
	private float _timerTemp;
	private bool startLogo = false;
	// VIDEO LOGO
	public RawImage _logo;
	public GameObject logo;
	private Texture2D texBlack;
	private Color32[] colBlack;
	public Material videoMat;
	// POINTS


	public enum Animazioni { LoopIdle, Preparing, LoadingFrames, FadeOut, StartLogoAnimation, EndLogoAnimation, End }
	public Animazioni animazioniGrafiche;

	public GameObject CountDownGameObject;
	private float xPosLeft, xPosRight;

	void Awake ()
	{
		CountDownGameObject.SetActive(false);

	}

	void Start () 
	{
		loading = new Texture2D[22];
		texBlack = new Texture2D(1920,1080, TextureFormat.RGB24, false);	
		colBlack = new Color32[1920 * 1080];



		// texture anim
		LoadGloveLines ();
		// Logo init to Off
		HackVideoLogoToBlack();

		xPosLeft = ForwardLeft.transform.position.x;
		xPosRight = ForwardRight.transform.position.x;
	}
	
	// Draw Graphic Stuff UI
	void Update () 
	{
		switch (animazioniGrafiche) 
		{
			case Animazioni.LoopIdle:
				LoopGloves (true, 1.29f, 1.29f, 0.23f, 0.23f);
				PulseButtonStartPerformace (true, 1.0f);
				DMXManager.actualMode = DMXManager.SetDmxMode.WhiteSound;
				break;

			case Animazioni.Preparing:
				//print ("WearTheGloves");
				Timer ();
			    LoopGloves (true, 1.29f, 1.29f, 0.23f, 0.23f);
			    PulseButtonStartPerformace (true, 1.0f);
				break;

			case Animazioni.LoadingFrames:
				// update loading frame, time velocity
				UpdateFrame (loading, 0.2f);
				DMXManager.actualMode = DMXManager.SetDmxMode.RedLoading;
				break;

			case Animazioni.FadeOut:
				//print ("Fade Out");
				FadeOutLoading (1.2f, 4);
				DMXManager.actualMode = DMXManager.SetDmxMode.Off;
				break;

			case Animazioni.StartLogoAnimation:
					//print("Logo Animation");
				StartVideoLogo ();

					//HACK 
				Trigger.triggerLeft = false;
				Trigger.triggerRight = false;
				panelGloves.SetActive (false);

				break;

			case Animazioni.EndLogoAnimation:
				isVideoLogoFinished ();
				DMXManager.actualMode = DMXManager.SetDmxMode.RedPulse;

				
				break;

		case Animazioni.End:
			print ("End Graphic");
						// Start CountDown 
			CountDownGameObject.SetActive (true);
			DMXManager.actualMode = DMXManager.SetDmxMode.Off;
			holdOn.text = "STARTING TRAINING";
			holdOn.enabled = true;
			holdOn.color = new Color (1, 1, 1, 1);

			Destroy (panelGloves);
			Destroy (logo);
			Destroy (_logo.gameObject);
			Destroy(GameObject.Find("Panel-StartPerformance"));
				


				Destroy (this.gameObject.GetComponent<GraphicDesign> ());

				break;
			

			default:
				break;
		}

	

	}

	// loop gloves animation
	public void LoopGloves(bool existLoop, float speedY, float speedX, float DistYRight, float DistYLeft)
	{
		existLoop = _existLoopGloves;

		if (existLoop) 
		{
			float _y = Mathf.Sin(Time.time * speedY) * DistYRight;
			float _yr = -Mathf.Cos(Time.time * speedX) * DistYLeft;
			float _yy = Mathf.Sin (Time.time * speedY * 3) * DistYLeft * 9;

			leftHand.transform.position = new Vector3 (leftHand.transform.position.x, leftHand.transform.position.y + _y, leftHand.transform.position.z);
			rightHand.transform.position = new Vector3 (rightHand.transform.position.x, rightHand.transform.position.y - _yr, leftHand.transform.position.z);

			rightGlove.transform.position = new Vector3 (rightGlove.transform.position.x, rightGlove.transform.position.y - _y, rightGlove.transform.position.z);
			leftGlove.transform.position = new Vector3 (leftGlove.transform.position.x, leftGlove.transform.position.y + _yr, leftGlove.transform.position.z);

			ForwardLeft.transform.position = new Vector3 (xPosLeft + Mathf.PingPong(Time.time, 3000), ForwardLeft.transform.position.y , ForwardLeft.transform.position.z);
			ForwardRight.transform.position = new Vector3 (xPosRight + _y, ForwardRight.transform.position.y , ForwardRight.transform.position.z);

		}

		else if (existLoop == false)
			panelGloves.gameObject.SetActive (false);
	}

	// pulse button start performace
	public void PulseButtonStartPerformace(bool existButtonStart, float timePulse)
	{
		existButtonStart = _existButtonStart;

		if (existButtonStart) 
		{
			int switchZeroToUno;
			switchZeroToUno = Convert.ToInt32 (changePulse); 

			startPerformaceButton.texture = (Texture2D)Resources.Load ("UI/StartPerformance/glove" + switchZeroToUno.ToString() ) as Texture2D;
			// temp pulse timer
			pulseTimer += Time.deltaTime;
			float t = ((int)pulseTimer);
			float remain = timePulse - t;

			if (t >= timePulse) 
			{
				pulseTimer = 0f;
				//print("pulse");
				changePulse = !changePulse;
			}

		}

		else if (existButtonStart == false) 
		{
			//startPerformaceButton.enabled = false;

			animazioniGrafiche = Animazioni.LoadingFrames;
		}
	}

	// CountDown Timer -60 sec
	public void Timer()
	{
		_timer += Time.deltaTime;
		float t = ((int)_timer);
		float remain = _duration - t;
		numCounter.text = remain.ToString ();

		if (t >= _duration) 
		{
			_timer = 0f;
			print("Preparing Game");
			// Disattivo elementi grafici
			numCounter.gameObject.SetActive (false);
			_existLoopGloves = false;
			_existButtonStart = false;
		}
	}

	// Caricamento frame guantoni
	public void UpdateFrame(Texture2D[] _frames, float velocity)
	{
		if (_activeLoading)
		{
			fCounter += Time.deltaTime / velocity;
			curFrame = ((int)fCounter);
			//print ("curFrame: " + curFrame);
			startPerformaceButton.texture = _frames [curFrame];

			// ############ AUDIO SOUNDgo ############ 
			AudioManager.LoadPlayAudioEffects ("Audio/otherSound/SOUNDgo", false);
		} 
		else if (_activeLoading == false) 
		{
			startPerformaceButton.texture = _frames [21];
			animazioniGrafiche = Animazioni.FadeOut;

		}

		// Last Frame loading
		if (curFrame == 20) 
		{
			//print ("last frame");
			_activeLoading = false;
			_alphaToZero = 0;
			holdOn.text = "LOADING";

			// ############ AUDIO SOUNDok ############ 
			AudioManager.LoadPlayAudioEffects("Audio/otherSound/SOUNDok", false);
			AudioManager.BackgroundVolume = 0;
		}
	}
		
	public void FadeOutLoading(float _fadeTime, int timeToStartLogo)
	{
		startPerformaceButton.color = new Color (1, 1, 1, Mathf.Lerp (startPerformaceButton.color.a, _alphaToZero, Time.deltaTime * _fadeTime));
		holdOn.color = new Color(1, 1, 1, Mathf.Lerp(holdOn.color.a, _alphaToZero, Time.deltaTime * (_fadeTime - 0.5f)));

		_timerTemp += Time.deltaTime;
		if (_timerTemp >= timeToStartLogo && !startLogo) 
		{
			holdOn.enabled = false;
			startLogo = true;
			//print ("startLogo " + startLogo);
			StartVideoLogo ();
		}
			
			
	}

	public void LoadGloveLines()
	{
		// preparo le texture per l'animazione loading

		for (int i = 0; i < loading.Length; i++) 
		{
			loading [i] = Resources.Load ("UI/Loading/gloveline" + i) as Texture2D;
		}
	}

	public void StartVideoLogo()
	{
		//DMXManager.actualMode = DMXManager.SetDmxMode.RedSound;

		logo.gameObject.SetActive (true);
		_logo.enabled = true;

		texBlack.anisoLevel = 0;
		texBlack.filterMode = FilterMode.Point;
		texBlack.Apply ();

		_logo.texture = texBlack;
		_logo.color = Color.white;
		_logo.material = videoMat;
		logo.GetComponent<UniversalMediaPlayer> ().OnPlayerTextureCreated (_logo.texture as Texture2D);
		logo.GetComponent<UniversalMediaPlayer> ().Play ();
		print ("Start Logo");

		animazioniGrafiche = Animazioni.EndLogoAnimation;

	}

	public void HackVideoLogoToBlack()
	{
		
		for (int i = 0; i < colBlack.Length; i++) 
		{
			colBlack [i] = new Color32 (0, 0, 0, 255);
		}

		texBlack.SetPixels32 (colBlack);
		//texBlack.anisoLevel = 0;
		texBlack.Apply ();

		_logo.GetComponent<RawImage> ().texture = texBlack;
		_logo.GetComponent<RawImage> ().color = Color.black;
		_logo.GetComponent<RawImage> ().material = null;
		logo.GetComponent<UniversalMediaPlayer>().Stop(false);
	}

	public void isVideoLogoFinished()
	{
		if (logo.GetComponent<UniversalMediaPlayer> ().Position >= 0.95) 
		{
			print ("End Video");
			_logo.enabled = false;
			animazioniGrafiche = Animazioni.End;
		}
	}

}
