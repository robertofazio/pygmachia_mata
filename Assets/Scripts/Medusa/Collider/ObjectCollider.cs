﻿using UnityEngine;
using System.Collections;

public class ObjectCollider : MonoBehaviour {

	private GameObject[] objectCollider;

	private Vector3[] velocity;
	private Vector3[] angularVelocity;

	public int sizeOf = 6;
	public float xGap = 5f;
	public float yGap = 2f;

	bool first = true;


	// Use this for initialization
	void Start () {
		CreateCollider ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		UpdateCollider ();
	}

	private void CreateCollider()
	{
		UnityEngine.Physics.gravity = new Vector3 (0, -0.98f, 0);

		angularVelocity = new Vector3[sizeOf];
		velocity = new Vector3[sizeOf];

		objectCollider = new GameObject[sizeOf];
		for (int i = 0; i < sizeOf; i++)
		{

			objectCollider [i] = GameObject.CreatePrimitive (PrimitiveType.Cube);
			objectCollider [i].transform.localScale = new Vector3 (2, 2, 2);
			objectCollider [i].transform.parent = this.transform;
			if (i % 2 == 0)
				objectCollider [i].transform.localPosition = new Vector3 (0 * xGap, i * yGap, 10);
			else
				objectCollider [i].transform.localPosition = new Vector3 (1 * xGap , i * yGap, 10);

			objectCollider [i].AddComponent<Rigidbody> ();
			objectCollider [i].GetComponent<Rigidbody> ().useGravity = false;
//			objectCollider [i].gameObject.AddComponent(typeof( DestroyCollider));
			objectCollider[i].name = "spawnElement";
//			objectCollider [i].GetComponent<Rigidbody> ().AddForce (100, 100, 100);
		}
	}

	private void UpdateCollider()
	{
		for (int i = 0; i < sizeOf; i++) 
		{
			if (objectCollider [i].transform.localPosition.y >= 0 && objectCollider[i].transform.localPosition.y < 1) 
			{
				objectCollider [i].GetComponent<Rigidbody> ().useGravity = true;
				objectCollider [i].GetComponent<Rigidbody> ().velocity = new Vector3 (0, 5, -9.5f);

			}
			else if( objectCollider[i].transform.localPosition.y > 0)
			{
				objectCollider [i].GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, -9.5f);
			}
		}



	}

}
