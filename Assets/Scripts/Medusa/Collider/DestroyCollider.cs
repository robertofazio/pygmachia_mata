﻿using UnityEngine;
using System.Collections;

public class DestroyCollider : MonoBehaviour 
{
	public static int cnt;

	void Start()
	{
		cnt = 0;
	}

	void OnCollisionEnter(Collision collider) 
	{
		if (collider.gameObject.tag == "SPAWN" && this.GetComponent<BoxCollider>().enabled == true) // 
		{
			//Debug.LogError ("Collision with: " + collider.gameObject.name + "Total Points: " + SaveScore.numPoints);

			collider.gameObject.GetComponent<BoxCollider> ().enabled = false;
			collider.gameObject.GetComponent<OggettoBehiavor> ().enabled = true;

			collider.gameObject.AddComponent<AudioSource> ();
			collider.gameObject.GetComponent<AudioSource> ().clip = Resources.Load ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Colpo-Oggetti") as AudioClip;
//			collider.gameObject.AddComponent<AudioEchoFilter> ();
//			collider.gameObject.GetComponent<AudioEchoFilter> ().decayRatio = 0.5f;
//			collider.gameObject.GetComponent<AudioEchoFilter> ().wetMix = 0.15f;
//			collider.gameObject.GetComponent<AudioEchoFilter> ().dryMix = 0.81f;
//			collider.gameObject.GetComponent<AudioEchoFilter> ().delay  = 145f;
			collider.gameObject.GetComponent<AudioSource> ().Play ();
			collider.gameObject.GetComponent<AudioSource> ().loop = false;

			DMXManager.actualMode = DMXManager.SetDmxMode.BluePulse;

		}


		cnt++;

		SaveScore.numPoints = cnt;

		//Debug.LogError (collider.gameObject.name);


	}
}
