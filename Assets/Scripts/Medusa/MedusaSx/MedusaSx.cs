﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


public class MedusaSx : MonoBehaviour {

	public static int numSeg = 12;
	private float minR = 0.5f;
	private float maxR = 1.0f;
	private float segDist = 1.5f; //PREVIOUS 0.15
	private Vector3 segOffset;
	public static GameObject[] segments;
	public float springiness = 0.9f;

	public Material mainMat;

	private bool bPunching = false;
	private float timePunchStart = 0.0f;
	private float timePunchStop = 0.0f;
	//punchTime controlla quanto dura l'animazione del pugno
	public float punchTime = 0.2f;
	//punchDist controlla quanto la medusa viaggia in avanti
	public float punchDist = 2.0f;


	public string	        	RemoteIP = "192.168.31.241";
	public static int 			SendToPort = 9000; 
	public static int 			ListenerPort = 8000; // Listener x-OSC
	public static Osc 			handler;
	public static UDPPacketIO 	udp;
	public static string 		address;

	private Vector3 xOscAcc = new Vector3(0.0f,0.0f,0.0f);
	//questi tengono traccia della storia dell'accelerazione: se aumenti la history sz, servira' un colpo piu' violento per sparare il pugno
	private const int xOscHistSz = 10;
	private int xOscHistId = 0;
	private float[] xOscAccMagHist;
	private float avgAcc = 1.0f;
	public static float dAccMag = 0.0f;

	public static bool punchLeft;
	//questa e' la soglia per il pugno, espressa in termini di delta magnitudine dell'accelerazione
	//ignora la direzione di G e del pugno, quindi spari sempre, senza preoccuparti di tenere traccia dell'IMU
	//valore piu' basso = maggiore sensibilita'
	public float punchThreshold = 1.0f;
	public GameObject quadSX;

	void Awake(){
		udp = gameObject.AddComponent<UDPPacketIO>() as UDPPacketIO;
		handler = gameObject.AddComponent<Osc>() as Osc;
	}

	// Use this for initialization
	void Start () {
		setupAcceleration ();

		setupOSC ();

		setSegDistance (segDist);
		makeSegments ();

		bPunching = false;
	}

	void setupAcceleration(){
		xOscAccMagHist = new float[xOscHistSz];
		for (int i=0; i<xOscAccMagHist.Length; i++) {
			xOscAccMagHist[i] = 1.0f;
		}
		xOscHistId = 0;
	}

	public void setupOSC(){
		udp.init(RemoteIP, SendToPort, ListenerPort);
		handler.init(udp);
		handler.SetAddressHandler("/guantonisx/imu", onOSC);
	}

	public void onOSC(OscMessage oscMessage){
		address = oscMessage.Address;
		if(address == "/guantonisx/imu"){
			xOscAcc.x = float.Parse(oscMessage.Values [3].ToString());	
			xOscAcc.y = float.Parse(oscMessage.Values [4].ToString());
			xOscAcc.z = float.Parse (oscMessage.Values [5].ToString ());

			float mag = xOscAcc.magnitude;
			//UnityEngine.Debug.LogError("ACC "+mag);

			dAccMag = Mathf.Abs (mag-avgAcc);

//			UnityEngine.Debug.LogError("AVG: "+avgAcc+" / NOW: "+mag);

			xOscAccMagHist[xOscHistId]=mag;
			xOscHistId++;
			if(xOscHistId>=xOscHistSz){
				xOscHistId = 0;
			}

			avgAcc = xOscAccMagHist[0];
			for(int i=1;i<xOscAccMagHist.Length;i++){
				avgAcc+=xOscAccMagHist[i];
			}
			avgAcc/=xOscAccMagHist.Length;


		}
	}


	public void setSegDistance(float sd){
		segDist = sd;
		segOffset = new Vector3 (0.0f,0.0f,-segDist*minR);
	}

	public void setNumSegments(int n){
		numSeg = n;
		makeSegments ();
	}
	
	private void makeSegments(){
		segments = new GameObject[numSeg];

		for (int i=0; i<numSeg; i++) 
		{
			//segments[i] = Instantiate(quadSX, this.transform.position, Quaternion.identity) as GameObject;
			segments[i] = GameObject.CreatePrimitive(PrimitiveType.Plane);

			float sc = minR;
			if(i>0){
				float pct;
				if(i<numSeg/2){
					pct = (float)i/(float)(numSeg-1); 
					sc = Mathf.Lerp(minR,maxR,pct);
				}else{
					pct = (float)(i-(numSeg/2))/(float)((numSeg-1)-(numSeg/2));
					sc = Mathf.Lerp(maxR,minR,pct);
				}
			}

//			segments[i].transform.localScale = new Vector3(sc,sc,0.02f);
			segments [i].transform.parent = this.transform;
			segments[i].transform.localPosition = new Vector3(0,0,-i*segDist*minR);
			segments [i].transform.eulerAngles = new Vector3 (0, 0,-45);
			segments [i].GetComponent<Renderer> ().material = mainMat;
//
			Destroy(segments [i].GetComponent<MeshCollider>());
//
			if (i == 0)
				segments [i].GetComponent<MeshRenderer> ().enabled = false;
//
			if (i == 1) 
			{
				segments [i].AddComponent<Rigidbody> ();
				segments [i].GetComponent<Rigidbody> ().isKinematic = true;
				segments [i].AddComponent<BoxCollider> ();
			//	segments [i].GetComponent<BoxCollider> ().isTrigger = true;
				segments [i].GetComponent<BoxCollider> ().size = new Vector3 (10, 1, 2);

				segments [i].AddComponent<DestroyCollider> ();



		}
		}
	}

	//riposiziona in un punto specifico
	public void moveTo(Vector3 newPos){
		segments[0].transform.localPosition = new Vector3(newPos.x,newPos.y,newPos.z);
	}

	//trasla di mov
	public void moveBy(Vector3 mov){
		Vector3 curPos = new Vector3(segments[0].transform.localPosition.x,segments[0].transform.localPosition.y,segments[0].transform.localPosition.z);
		curPos += mov;
		moveTo (curPos);
	}

	//punching
	public void punch(){
		if (bPunching)return;
		bPunching = true;
		timePunchStart = Time.time;
		timePunchStop = timePunchStart + punchTime;
		moveBy (new Vector3(0,0,punchDist));
	}

	void updatePunch(){
		if (!bPunching)return;
		if (Time.time > timePunchStop) {
			bPunching = false;
			moveBy (new Vector3(0,0,-punchDist));
			dAccMag = 0.0f;
		}
	}

	private void updateTrail(){
		for (int i=1; i<segments.Length; i++) {
			updateSegment (i);
		}
	}

	private void updateSegment(int sId){
		if (sId < 1)return;
		Vector3 anchorPos = segments[sId-1].transform.localPosition+segOffset;
		Vector3 myPos = segments[sId].transform.localPosition;
		Vector3 newPos = (springiness * myPos)+((1.0f-springiness)*anchorPos);
		segments [sId].transform.localPosition = newPos;
	}

	// Update is called once per frame
	void Update () 
	{
		// HACK shield alpha a 0
		if (dAccMag > 1.5 && Shield.v == 0) 
		{
			punchLeft = true;
			punch ();
		}

		updatePunch ();
		updateTrail ();
	}

	
}
