﻿using UnityEngine;
using System.Collections;

public class OggettoBehiavor : MonoBehaviour 
{
	private float timer;
	private Color actualColor;

	private float alphaValue = 255;


	void Start ()
	{
		timer = 0;

		if (this.gameObject.name == "oggetto4(Clone)") 
			actualColor = this.GetComponent <Renderer> ().material.GetColor ("_Color");
		else
			actualColor = this.GetComponentInChildren <Renderer> ().material.GetColor ("_Color");
	

		this.GetComponent<OggettoBehiavor> ().enabled = false;
	}

	void Update () 
	{
		timer += Time.deltaTime * 2;

		if (alphaValue != 0) 
		{
			alphaValue = Mathf.Lerp (actualColor.a * 255, 0, timer);

			if (this.gameObject.name == "oggetto4(Clone)") 
			{
				this.gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", new Color (actualColor.r, actualColor.g, actualColor.b, alphaValue / 255));
				//Debug.LogError (this.gameObject.name);
			} 
			else 
			{
				foreach (Transform child in this.transform) 
				{
					child.gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", new Color (actualColor.r, actualColor.g, actualColor.b, alphaValue / 255));
				}
			}
				
		} 
	
		else 
		{
			DMXManager.actualMode = DMXManager.SetDmxMode.OffAndSetRedSoundMedusa;
			Destroy (this.gameObject);

		}


	}
}
