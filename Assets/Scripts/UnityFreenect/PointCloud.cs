﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class PointCloud : MonoBehaviour {

	public enum CollapseState{
		Expanded,
		Collapsing,
		Collapsed,
		Expanding
	};

	public enum InOutState{
		In,
		Out,
		Entering,
		Exiting
	};

	public enum ExplodeImplodeState
	{
		Explode,
		Implode,
		Exploding,
		Imploding
	};

	//private Mesh mesh;
	private int resW = 640;
	private int resH = 480;

	public static int stepW = 1;
	public static int stepH = 10; // precedente:10 (divisibile x480)

	private static int cloudW;	
	private static int cloudH;

	private int numPoints;
	public static Vector3[] pts;

	public static float timerSin = 0;
	public float durataSin = .5f;
	private float zPos = 0;
	public static float heightScaleSin = 0;
	private float noiseScaleSin = 0;
	public static float startSin;

	private int[] idx;
	public static Color[] cls;
	public static Color[] pix;
	private float[] yStart;
	private float[] yCur;
	public float zScale = 200.0f;

	private float startTimeC = 0.0f;
	private float stopTimeC = 0.0f;

	public float collapseTime = 0.0f;
	//VIENE SETTATA IN UPDATE AL PRIMO AVVIO

	private float startTimeIo = 2.0f;
	private float stopTimeIo = 0.0f;
	public float inOutTime = 1.0f;
	public float outZ = 1000.0f;
	private float curZout = 0.0f;
	private float yCollapsed;
	private CollapseState collapseState;
	private InOutState inOutState;
	public ExplodeImplodeState explodeImplodeState;

	public float noiseAdder = 0.1f;
	public float noiseMultiplier = 100.0f;
	private float curNoise = 0.0f;

	private bool bNewFrame = false;
	public static bool triggerSin = true;
	public bool bNoise = false;


	public GameObject MedusaDx;
	public GameObject MedusaSx;


	//NUOVA PARTE PARTICLE SYSTEM
	public ParticleSystem psKinect;
	public static ParticleSystem.Particle[] particlesArray; 

	public static float xGapValue;
	public static float yGapValue;
	private float maxGap = 3;

	public float spacing = 1f;

	private int particleW;
	public static int particleH;

	public static int centerOfPointClouds;
	public static Transform transformParticleKinect;

	public float noiseScale = 6.0f; // 0.05f
	public float heightScale;
	private float perlinNoiseAnimX = 0.01f; 
	private float perlinNoiseAnimY = 0.01f;

	public float _heightScaleEnding;
	public float _startHeightScaleValue;

	public float _timer;
	public float _durationLerp = 2.0f; // durata del lerp iniziale del particle
	public bool fadeOutClouds = false;
	private float size= 1.35f;

	private bool first = true;
	private float zGapExplodeState =0;
	private float maxExplode = 900f;
	private float _timerExplodeImplode;
	public float _startTime;
	public bool _climaxNoise;

	public static bool boolInstantiateLeft = false;
	public static bool boolInstantiateRight = false;


	void Awake ()
	{
		
		psKinect.gameObject.SetActive (true);

		// Punti visualizzati in righe e colonne
		particleW = 640;
		particleH = 480 / stepH;

		centerOfPointClouds = (particleH / 2 * particleW) + ((particleH / 2 + 1) * (particleW / 2));

		// Hardcode position
		psKinect.transform.position = new Vector3 (320.0f, 240.0f, 240.0f);
		psKinect.transform.rotation = new Quaternion (0.0f, 0.0f, 180.0f, 0.0f);

		transformParticleKinect = psKinect.transform;
	}


	void Start () 
	{
		_timer = 0;

		CreateMesh (640,480, stepW, stepH);

		bNewFrame = false;
		curZout = 0.0f;

		collapseState = CollapseState.Expanded;
		toggleCollapse ();

		//inOutState = InOutState.In;

		setNoise (false);

		_startHeightScaleValue = 200f;
		xGapValue = maxGap;
		yGapValue = 0;

		_heightScaleEnding = 0f;
		heightScale = _heightScaleEnding;

		particlesArray = new ParticleSystem.Particle[particleH * particleW]; 
		psKinect.maxParticles = particleH * particleW;       
		psKinect.Emit(particleH * particleW);       
		psKinect.GetParticles(particlesArray);



	}

	void CreateMesh(int _resW, int _resH, int _stepW, int _stepH)
	{
		resW = _resW;
		resH = _resH;
		stepW = _stepW;
		stepH = _stepH;
		cloudW = 0;

		for (int i=0; i<resW; i+=stepW) 
		{
			cloudW++;
		}
		cloudH = 0;

		for (int i=0; i<resH; i+=stepH) 
		{
			cloudH++;
		}

		numPoints = cloudW * cloudH;

		pts = new Vector3[numPoints];
		idx = new int[numPoints];
		cls = new Color[numPoints];

		yStart = new float[cloudW];
		yCur = new float[cloudW];

		for (int yy=0; yy<cloudH; yy++) 
		{
			yStart[yy] = yy*stepH;
			yCur[yy] = yStart[yy];

			for(int xx=0;xx<cloudW;xx++)
			{
				int ii = yy * cloudW + xx;
				pts[ii] = new Vector3(xx,yy*stepH,0.0f);
				idx[ii] = ii;
				cls[ii] = new Color(1.0f,1.0f,1.0f);
			}
		}

		yCollapsed = (float)resH / 2.0f;
	}

	void Update ()
	{

			updateInOutAnim ();
			updateCollapseAnim ();

			if (collapseState == CollapseState.Collapsed && first) {

				first = false;
				zGapExplodeState = maxExplode;
				collapseTime = _durationLerp * 2;
				//psKinect.gameObject.SetActive (false);
				explodeImplodeState = ExplodeImplodeState.Implode;

				//GameManager.kinectStuffActive = false;

			}

			if (bNewFrame) {
				for (int yy = 0; yy < cloudH; yy++) {
					float ry = yCur [yy];
					float nn = 0.0f;

					// Draw Particles
					for (int xx = 0; xx < cloudW; xx++) {
						int ii = yy * cloudW + xx;
						int ip = (yy * stepH) * resW + (xx * stepW);


						//***** PUNCH NOISE TRIGGER ******
						noiseScaleSin = 0.02f;
						zPos = Mathf.PerlinNoise (xx * noiseScaleSin + perlinNoiseAnimX, yy * noiseScaleSin + perlinNoiseAnimY);

						pts [ii].z = ((pix [ip].r * zScale) + zGapExplodeState) + (zPos * heightScaleSin);
						pts [ii].y = ry + ry * yGapValue + nn;
						pts [ii].x = xx - (cloudW / 2 - xx) * xGapValue;

						particlesArray [ii].position = pts [ii];
						//particlesArray [ii].size = 2;
						if (explodeImplodeState == ExplodeImplodeState.Imploding && _timer != 0f) {
							particlesArray [ii].startSize = size;
						} 
						Color32 min = new Color (255, 0, 0, 255);
						Color32 max = new Color (0, 0, 255, 255);

						particlesArray [ii].startColor = Color32.Lerp (min, max, pix [ip].r);

						if (bNoise) {
							curNoise += noiseAdder;
							nn = Mathf.PerlinNoise (yy * noiseScale + perlinNoiseAnimX, xx * noiseScale + perlinNoiseAnimY) * heightScale;
						}

						if (fadeOutClouds) {
							nn = Mathf.PerlinNoise (yy * noiseScale + perlinNoiseAnimX, xx * noiseScale + perlinNoiseAnimY) * heightScale;
						}
					}
				}
			}


			// TIMER PUNCH NOISE
			if (!triggerSin && timerSin < durataSin) {
				timerSin += Time.deltaTime;
				heightScaleSin = Mathf.Lerp (startSin, 150, timerSin / durataSin);
			} else if (!triggerSin && timerSin >= durataSin) {
				timerSin = 0;
				triggerSin = true;
			} else if (triggerSin && heightScaleSin != 0)
				heightScaleSin = Mathf.Lerp (heightScaleSin, 0, Time.deltaTime / durataSin);



			//******** START NOISE & END NOISE STUFF ********
			if (bNoise || fadeOutClouds == true)
				_timer += Time.deltaTime;
			//fine
			if (fadeOutClouds == true || bNoise) {
			
				if (_climaxNoise)
					heightScale = Mathf.Lerp (_heightScaleEnding, _startHeightScaleValue, _timer / _durationLerp);
				else
					heightScale = Mathf.Lerp (_startHeightScaleValue, _heightScaleEnding, _timer / _durationLerp);


				if (heightScale == _startHeightScaleValue) {
					_climaxNoise = false;
					_timer = 0;
				} else if (heightScale == _heightScaleEnding)
					_timer = 0;
			} else {
				fadeOutClouds = false;
				bNoise = false;
				_timer = 0;
			}

			if (explodeImplodeState == ExplodeImplodeState.Imploding && _timer != 0)
				size -= .002f;
			

	
			perlinNoiseAnimX += 0.01f; 
			perlinNoiseAnimY += 0.01f;  



			//****** FINAL SET *******


			psKinect.SetParticles (particlesArray, particlesArray.Length);

			bNewFrame = false;

			if (explodeImplodeState == ExplodeImplodeState.Exploding || explodeImplodeState == ExplodeImplodeState.Imploding)
				explodeImplode ();




	}

	public int getCloudWidth(){return cloudW;}
	public int getCloudHeight(){return cloudH;}

	public float map(float s, float a1, float a2, float b1, float b2){
		return b1 + (s-a1)*(b2-b1)/(a2-a1);
	}

	public void setNoise(bool b){
		bNoise = b;
		if (bNoise)
			curNoise = 0.0f;
	}
	public void toggleNoise(){
		setNoise (!bNoise);
	}

	public bool isNoiseRunning(){
		return bNoise;
	}

	public InOutState getInOutState(){
		return inOutState;
	}

	public CollapseState getCollapseState(){
		return collapseState;
	}

	public bool isCollapseAnimating(){
		return (collapseState==CollapseState.Collapsing || collapseState==CollapseState.Expanding);
	}

	public bool isIn(){
		return inOutState != InOutState.Out;
	}

	public void goIn(){
		if (inOutState != InOutState.Out)return;
		inOutState = InOutState.Entering;
		startTimeIo = Time.time;
		stopTimeIo = startTimeIo + inOutTime;
		curZout = outZ;
	}

	public void goOut(){
		if (inOutState != InOutState.In)return;
		inOutState = InOutState.Exiting;
		startTimeIo = Time.time;
		stopTimeIo = startTimeIo + inOutTime;
		curZout = 0.0f;
	}

	public void toggleInOut(){
		switch (inOutState) {
		case InOutState.In:
			goOut();
			break;
		case InOutState.Out:
			goIn();
			break;
		default:
			break;
		}
	}

	public void startCollapse(){
		if (collapseState == CollapseState.Collapsed)return;
		startTimeC = Time.time;
		stopTimeC = startTimeC + collapseTime;
		for (int i=0; i<yCur.Length; i++) {
			yCur[i]=yStart[i];
		}
		collapseState = CollapseState.Collapsing;
	}

	public void startExpand(){
		if (collapseState == CollapseState.Expanded)return;
		startTimeC = Time.time;
		stopTimeC = startTimeC + collapseTime;
		for (int i=0; i<yCur.Length; i++) {
			yCur[i]=yCollapsed;
		}
		collapseState = CollapseState.Expanding;
	}

	public void toggleCollapse(){
		switch (collapseState){
		case CollapseState.Collapsed:
			startExpand();
			break;
		case CollapseState.Expanded:
			startCollapse();
			break;
		default:
			break;
		}
	}

	public void setFrame(Texture2D newFrame){
		pix = newFrame.GetPixels ();
		bNewFrame = true;

		//mesh.vertices = pts;
	}
	

	private void updateCollapse(){
		float now = Time.time;
		if (now < stopTimeC) {
			for (int i=0; i<yCur.Length; i++) {
				yCur[i]= map (now,startTimeC,stopTimeC,yStart[i],yCollapsed);
			}
		} else {
			for (int i=0; i<yCur.Length; i++) {
				yCur[i]=yCollapsed;
			}
			collapseState=CollapseState.Collapsed;
		}
	}

	private void updateExpand(){
		float now = Time.time;
		if (now < stopTimeC) {
			for (int i=0; i<yCur.Length; i++) {
				yCur[i]= map (now,startTimeC,stopTimeC,yCollapsed,yStart[i]);
			}
		} else {
			for (int i=0; i<yCur.Length; i++) {
				yCur[i]=yStart[i];
			}
			collapseState=CollapseState.Expanded;
		}
	}

	public void updateCollapseAnim(){
		switch (collapseState) {
		case CollapseState.Collapsing:
			updateCollapse();
			break;
		case CollapseState.Expanding:
			updateExpand();
			break;
		default:
			break;
		}
	}

	private void updateIn(){
		float now = Time.time;
		if (now < stopTimeIo) {
			curZout = map (now,startTimeIo,stopTimeIo,outZ,0.0f);
			setZ(-curZout);
		} else {
			inOutState=InOutState.In;
			curZout=0.0f;
			setZ (curZout);
		}

	}

	private void updateOut(){
		float now = Time.time;
		if (now < stopTimeIo) {
			curZout = map (now,startTimeIo,stopTimeIo,0.0f,outZ);
			setZ (-curZout);
		} else {
			inOutState=InOutState.Out;
			curZout=outZ;
			setZ(-curZout);
		}

	}

	public void updateInOutAnim(){
		switch (inOutState) {
		case InOutState.Entering:
			updateIn();
			break;
		case InOutState.Exiting:
			updateOut();
			break;
		default:
			break;
		}
	}

	public void setZ(float _z){
		Vector3 curP = transform.localPosition;
		curP.z = _z;
		transform.localPosition = curP;
	}
	


	public void explodeImplode()
	{
		//Debug.LogError ("explodeImplode");

		if (explodeImplodeState == ExplodeImplodeState.Exploding)
		{
			if (zGapExplodeState == 0)
			{
				explodeImplodeState = ExplodeImplodeState.Explode;
				_timerExplodeImplode =0;
				bNoise = false;
				_timer = 0;

			}
			else 
			{

				_timerExplodeImplode = (Time.time - _startTime) / collapseTime;
				xGapValue = Mathf.Lerp (maxGap , 0, _timerExplodeImplode);
				zGapExplodeState = Mathf.Lerp ( maxExplode,0, Mathf.SmoothStep (0,1, _timerExplodeImplode));
			}
			
		}
		else if( explodeImplodeState == ExplodeImplodeState.Imploding)
		{
			if (zGapExplodeState == maxExplode)
			{
				explodeImplodeState = ExplodeImplodeState.Implode;
				_timerExplodeImplode = 0;
				fadeOutClouds = false;
				_timer = 0;

				//SCENA MEDUSA
				psKinect.gameObject.SetActive (false);
				//MedusaDx.SetActive (true);
				//MedusaSx.SetActive (true);
				GameObject.Find ("GAME MANAGER").GetComponent<GameManager> ().PygmachiaMode (GameManager.Statement.Medusa);


			} 
			else
			{
				_timerExplodeImplode = (Time.time - _startTime) / collapseTime;
				xGapValue = Mathf.Lerp (0 , maxGap, _timerExplodeImplode);
				zGapExplodeState = Mathf.Lerp (0, maxExplode, Mathf.SmoothStep (0,1, _timerExplodeImplode));
			}

		}
	}


	void ChangeStatus()
	{
		/*if (Input.GetKeyDown (KeyCode.C)) 
		{
			if(!isCollapseAnimating())
			{
				if(getCollapseState()==PointCloud.CollapseState.Expanded)
				{
					UnityEngine.Debug.LogError ("collpase");
				}else
				{
					UnityEngine.Debug.LogError ("expand");
				}
			}
			toggleCollapse ();
		} else if (Input.GetKeyDown (KeyCode.I)) {
			if(isIn()){
				UnityEngine.Debug.LogError ("out");
			}else{
				UnityEngine.Debug.LogError ("in");
			}
			toggleInOut ();
		} else if (Input.GetKeyDown (KeyCode.N)) {
			UnityEngine.Debug.LogError ("noise");
			toggleNoise ();
		}*/
	}




}
