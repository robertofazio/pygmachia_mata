﻿/* Roberto Fazio Studio 2016
 * Enable Unsafe code on MONODEVELOP 5.9
 * last update 09 giugno 2016 
 * https://groups.google.com/forum/#!topic/openkinect/Kfjx3MRshHA
 * LoadRawData : http://answers.unity3d.com/questions/555984/can-you-load-dds-textures-during-runtime.html
 * Doesn't work in Editor(crash), only work in Build x86_64 Linux 
 * -------------------- RENDER TEXTURE ---------------------------------------------------------------------------------------------------
 * Depth Textures Unity http://docs.unity3d.com/Manual/SL-DepthTextures.html
 * http://www.danielkiedrowski.com/2012/02/unity3d-how-to-render-depth-values-and-normals-to-a-rendertexture/
 * -------------------- CONVERT DEPTH FRAME FUNCTION ------------------------------------------------------------------------------------
 * https://digitalerr0r.wordpress.com/2011/06/21/kinect-fundamentals-3-getting-data-from-the-depth-sensor/
 * -------------------- KINECT ----------------------------------------------------------------------------------------------------------
 * http://www.i-programmer.info/ebooks/practical-windows-kinect-in-c/3802-using-the-kinect-depth-sensor.html
 * https://msdn.microsoft.com/en-us/library/jj131029.aspx
 * http://www.i-programmer.info/ebooks/practical-windows-kinect-in-c.html
 * 
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using freenect;
using System.Threading;
using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;



public class UnityKinect : MonoBehaviour 
{
	public Image RawKinectDepthImage;

	public static Kinect kinect;
	private int _w, width;
	private int _h, height;
	private byte[] pixels; 


	private Texture2D texDepth;
	private Color32[] cols;

	private short[] raw; // array di short 16bit della Depth Kinect
	private ushort[] uRaw; // array di conversione

	public PointCloud pointCloud;
	public static bool view;
	public static bool pc;

	void Start ()
	{
		InitKinect ();
		    
		texDepth = new Texture2D (640, 480, TextureFormat.ARGB32, false); // Depth 11Bit 640x480
		// Depth Map View Active
		view = true;
		pc = true;
		//pointCloud.gameObject.SetActive (pc);
	}

	private void HandleKinectDepthCameraDataReceived (object sender, DepthCamera.DataReceivedEventArgs e) 
	{
		//depthCamInfo.text = "Depth ricevuta a "+ e.Timestamp.ToString();

		IntPtr handleDepth = e.Data.DataPointer;

		int w = e.Data.Width;  // 640
		int h = e.Data.Height; // 480
		width = w;
		height = h;
		// The DepthImageFrame data is presented as an array of short integers
		// i.e. each element is 16 bits, which is different from the byte array used in the VideoImageFrame.
		raw = new short[width * height]; 
		uRaw = new ushort[width * height];
		// Copia i dati da una matrice di short con segno a 16 bit gestita e unidimensionale a un puntatore di memoria non gestita https://msdn.microsoft.com/it-it/library/ms146628(v=vs.110).aspx
		Marshal.Copy (e.Data.DataPointer, raw, 0, raw.Length);

		// Preparo i colori 
		cols = new Color32[raw.Length];

		// ciclo nella depth grezza
		for(int i = 0; i < raw.Length; i++)
		{
			// assegno a ushort[] gli short 16bit provenienti dalla DepthCam
			uRaw[i] = (ushort)(raw[i]);

			cols [i].a = 255;
			cols [i].r = (byte)uRaw [i];
			cols [i].g = (byte)uRaw [i];
			cols [i].b = (byte)uRaw [i];


		}

	}
		
	void InitKinect()
	{

		if (Kinect.DeviceCount > 0) 
		{
			// Try to open a device
			kinect = new Kinect (0);
			kinect.Open ();
			//WriteLogFile ("Kinect is Open: " + kinect.IsOpen);
			//preparo le funzioni che ricevono roba dalle camere
			kinect.DepthCamera.DataReceived += HandleKinectDepthCameraDataReceived;

			//accendi le camere
			//kinect.VideoCamera.Start();
			kinect.DepthCamera.Start();

			// Try to set LED colors
			kinect.LED.Color = LEDColor.Red;
			//kinect.Motor.Tilt = 0;

			//mostra video modes per incodare disponibili 
			int i=0;
			foreach(var mode in kinect.VideoCamera.Modes)
			{
				switch (mode.Format) 
				{
					case VideoFormat.RGB:
						//WriteLogFile("Video Mode "+i+": RGB");
						break;
					case VideoFormat.Infrared8Bit:
					    //WriteLogFile("Video Mode "+i+": IR 8bit");
						break;
					case VideoFormat.Infrared10Bit:
					    //WriteLogFile("Video Mode "+i+": IR 10bit");
						break;
					default:
						break;
				}
				i++;
			}
				


			//mostra depth modes
			i = 0;
			foreach(var mode in kinect.DepthCamera.Modes)
			{
				switch(mode.Format)
				{
					case DepthFormat.Depth10Bit:
						//WriteLogFile("Depth Mode "+i+": 10 bit");
						break;
					case DepthFormat.Depth11Bit:
						//WriteLogFile("Depth Mode "+i+": 11 bit");
						break;
					default:
						break;
				}
				i++;
			
			}

			// set the DepthCam and VideoCam Format
			//kinect.DepthCamera.Mode.Format = DepthFormat.Depth10Bit;

			//Thread.Sleep (3000);
		}
	}
		
	void Update ()
	{

	
			//aggiorna sensori e motore
			kinect.UpdateStatus ();
			//aggiorna gli eventi
			Kinect.ProcessEvents ();
	
			// Draw DepthCamera 
			texDepth.SetPixels32 (cols); // SetPixels32 works only on ARGB32 texture formats. For other formats SetPixels32 is ignored

			texDepth.Apply ();
		//	RawKinectDepthImage.material.mainTexture = texDepth;

//			if (view)
//				RawKinectDepthImage.gameObject.SetActive (true);
//
//			if (!view)
//				RawKinectDepthImage.gameObject.SetActive (false);
		
		//	RawKinectDepthImage.material.mainTexture = texDepth;

			pointCloud.setFrame (texDepth);


#region KEY
			// Draw DepthMap on Image UI
			//if (Input.GetKeyDown (KeyCode.D)) 
			//	view = !view;
		
		if (Input.GetKeyDown (KeyCode.Keypad1)) 
		{
				kinect.LED.Color = LEDColor.BlinkGreen;
				//kinect.Motor.Tilt = 0;
				// Shutdown the Kinect context
				kinect.Close ();
				Kinect.Shutdown ();

				Destroy (GameObject.Find ("GAME MANAGER").gameObject);
				Destroy (GameObject.Find ("Main Camera").GetComponent<MovCamera> ());
				Destroy (GameObject.Find ("AUDIO"));
				Destroy (GameObject.Find ("VIDEOLOGO"));
				Destroy (GameObject.Find ("DMX"));
				Destroy (GameObject.Find ("MEDUSASTUFF"));		

				GameManager._actualGame = GameManager.Statement.Idle;

				SceneManager.LoadScene ("Start");
			}



#endregion


	
	}
		



	public static void _print(string msg)
	{
		//UnityEngine.Debug.LogError (msg);
		//WriteLogFile (msg);
	}

	void OnApplicationQuit()
	{
		//WriteLogFile("Close Kinect and quit application " + Time.time + " seconds");
		if (Kinect.DeviceCount > 0 ) 
		{
			kinect.LED.Color = LEDColor.BlinkGreen;
			kinect.Motor.Tilt = 0;
			// Shutdown the Kinect context
			kinect.Close ();
			Kinect.Shutdown();

			GameManager._actualGame = GameManager.Statement.Idle;
		}


	}

}
