﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;
using System;
using freenect;


public class GameManager : MonoBehaviour 
{   
	public enum Statement { Idle, WearingGloves, StartGame, FadeOutKinectScene, Medusa, End, PhotoBook }
	public static Statement game;
	public static Statement _actualGame;

	public GameObject UnityKinectGameObject;
	public GraphicDesign graphicAsset;

	public GameObject[] MedusaGameObjects;
	public GameObject MEDUSASTUFF;

	public Text Points;
	public GameObject panelPunteggio;

	public static bool kinectStuffActive = true;

	private float mainTimer;
	private float reset = 120.0f;
	private bool endTime;

	public GameObject psk;

	void Awake ()
	{
		Application.targetFrameRate = 30;

		Camera.main.GetComponent<Bloom> ().enabled = false;
		panelPunteggio.gameObject.SetActive (false);

		this.gameObject.GetComponent<EndPerformance> ().enabled = false;
	}

	void Start ()
	{
		Cursor.visible = false;
		kinectStuffActive = false;

		// Freenect doesn't works on Editor
		#if UNITY_EDITOR
			GameObject.Find("KINECT").SetActive(false);
			//UnityKinectGameObject.GetComponent<UnityKinect>().enabled = false; 
			UnityKinectGameObject.GetComponent<PointCloud> ().gameObject.SetActive (false);

		#elif UNITY_STANDALONE_LINUX 
			UnityKinectGameObject.SetActive(true); 
			UnityKinectGameObject.GetComponent<UnityKinect>().enabled = true; 
			UnityKinectGameObject.GetComponent<PointCloud> ().psKinect.gameObject.SetActive (true);	
			//Debug.LogError (Application.platform);
		#endif

		game = Statement.Idle;

		print (game);

		// Disable Medusa at Start
		MedusaGameObjects[0].SetActive (false);
		MedusaGameObjects[1].SetActive (false);
		MEDUSASTUFF.gameObject.SetActive (false);

		Camera.main.GetComponent<MovCamera> ().enabled = false;

		//CrashDetect.SetExePoint ("ciccio");
		Camera.main.farClipPlane = 500;

		mainTimer = 0.0f;
		endTime = false;

		//WriteConsoleSend.WriteLogFile ("PygmachiaDev Application Started");

	}

	public void PygmachiaMode (Statement game)
	{
		switch (game) 
		{
			case Statement.Idle:
				_actualGame = game;
				print ("Idle");
				graphicAsset.animazioniGrafiche = GraphicDesign.Animazioni.LoopIdle;
			   // DMXManager.actualMode = DMXManager.SetDmxMode.WhiteSound;

				break;

			case Statement.WearingGloves:
			
				graphicAsset.animazioniGrafiche = GraphicDesign.Animazioni.Preparing;
			    DMXManager.actualMode = DMXManager.SetDmxMode.RedPulse;
				break;

			case Statement.FadeOutKinectScene:
				FadingOutKinect ();
				break;

			case Statement.Medusa:
				_actualGame = game;
				kinectStuffActive = false;
				Medusa ();
				break;
		
			default:
				break;
		}

		//return game;
	}

	void Update () 
	{
		TRIGGERSTART ();
		//FrameRate ();
		Points.text = DestroyCollider.cnt.ToString();

//		mainTimer += Time.deltaTime;
//		//Debug.LogError (mainTimer);
//
//		if (mainTimer >= reset && _actualGame == Statement.Idle) 
//			endTime = true;

		if(endTime == true)
		{
			

			UnityKinect.kinect.LED.Color = LEDColor.BlinkGreen;
			UnityKinect.kinect.Close ();
			Kinect.Shutdown ();

			Destroy (GameObject.Find ("GAME MANAGER").gameObject);
			Destroy (GameObject.Find ("Main Camera").GetComponent<MovCamera> ());
			Destroy (GameObject.Find ("AUDIO"));
			Destroy (GameObject.Find ("VIDEOLOGO"));
			Destroy (GameObject.Find ("DMX"));
			Destroy (GameObject.Find ("MEDUSASTUFF"));		

			GameManager._actualGame = GameManager.Statement.Idle;

			endTime = false;
			mainTimer = 0.0f;

			SceneManager.LoadScene ("Start");


		}
	
	}

	void TRIGGERSTART()
	{
		// TRIGGER che simula lo start performance dell'IMU
		if (Input.GetKeyDown (KeyCode.Return) || Trigger.startGame == true) 
		{
			PygmachiaMode(Statement.WearingGloves);
			Trigger.startGame = false;
		}
	}

	public void FrameRate()
	{
		float Fps = 120;
		float interp = Time.deltaTime / (0.5f + Time.deltaTime);
		float currentFPS = 1.0f / Time.deltaTime;
		Fps = Mathf.Lerp(Fps, currentFPS, interp);
		//frameRate.text = Fps.ToString ("N0");
	}

	public void FadingOutKinect()
	{
		UnityKinectGameObject.GetComponent<PointCloud>().fadeOutClouds = true;
		UnityKinectGameObject.GetComponent<PointCloud>()._startTime = Time.time;
		UnityKinectGameObject.GetComponent<PointCloud> ().explodeImplodeState = PointCloud.ExplodeImplodeState.Imploding;
		UnityKinectGameObject.GetComponent<PointCloud>()._climaxNoise = true;
		UnityKinectGameObject.GetComponent<PointCloud> ()._durationLerp = 1.75f;
		UnityKinectGameObject.GetComponent<PointCloud> ().collapseTime = UnityKinectGameObject.GetComponent<PointCloud> ()._durationLerp * 2;
		UnityKinectGameObject.GetComponent<PointCloud> ().toggleCollapse ();

		DMXManager.actualMode = DMXManager.SetDmxMode.Off;
		Camera.main.GetComponent<Bloom> ().enabled = true;



	}

	public void Medusa()
	{

		UnityKinect.kinect.LED.Color = freenect.LEDColor.BlinkGreen;
		UnityKinect.kinect.Close ();
		Kinect.Shutdown ();

		Destroy (UnityKinectGameObject);
		Destroy (psk);


		//Debug.LogError ("Scena Medusa");
		Destroy (GameObject.Find ("START").gameObject);

		MedusaGameObjects[0].SetActive (true);
		MedusaGameObjects[1].SetActive (true);
		MEDUSASTUFF.gameObject.SetActive (true);

		AudioManager.LoadPlayAudioMedusa("Audio/Mata-SoundTrack/MaTa-SoundTrack-BackGround-4", false, 0.3f);
		AudioManager.BackgroundVolume = 0.5f;
		AudioManager.PygmachiaMainAudioSource.volume = 0.5f;


		Pavimento._pavimento = true;
		Soffitto._soffitto = true;

		SpawObjects.triggerStartSpawin = true;
		DMXManager.actualMode = DMXManager.SetDmxMode.RedSound;

		panelPunteggio.gameObject.SetActive (true);

		//Destroy(UnityKinectGameObject

	}

	void OnApplicationQuit()
	{
		//WriteConsoleSend.WriteLogFile ("PygmachiaDev Application Closed");
		//WriteConsoleSend.SendEmail ();
	}
}
