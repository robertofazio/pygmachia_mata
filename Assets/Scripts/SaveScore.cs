﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class SaveScore : MonoBehaviour 
{
	public static string st = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	public static string rndID;
	public static int rndIndex;

	public static StreamWriter streamFile;
	public static int waitTime = 1;
	public static int refreshTime = 10;
	public static WWW loadFile;

	public static int numPoints;
	public Text Id;
	//public static string ScoreSavedFile = "http://localhost:49465/PygmachiaFacebook/ScoreSaved.txt";
	//public static string tempString = "http://localhost:49465/Score.txt";

	void Start () 
	{
		GenerateRandomID(8);

		//if(File.Exists("ScoreSaved.txt"))
			//File.Delete("ScoreSaved.txt");

		Id.text = SaveScore.rndID.ToString ();

		//Get_ID_SaveFile();

	}
		
	public static void GenerateRandomID(int maxLenghtID)
	{
		rndID = "";
		rndIndex = Random.Range(0,st.Length);

		for(int i = 0; i < maxLenghtID; i++)
		{
			rndID += st[Random.Range(0,st.Length)];
		}
			
	}
	
	// Salvo il File nell'indirizzo dell'applicazione UnityWebGL
	public static void Get_ID_SaveFile()
	{
		if(File.Exists("TakePhoto.txt"))
			File.Delete("TakePhoto.txt");

		if(File.Exists("ScoreSaved.txt"))
			File.Delete("ScoreSaved.txt");
		
		streamFile = new StreamWriter("ScoreSaved.txt");
		streamFile.Write("Player ID:  "+ rndID + " Points: " + DestroyCollider.cnt.ToString ());
		streamFile.Close();
		//Debug.LogError("File Saved");
	}
		
	void OnApplicationQuit()
	{
		//Get_ID_SaveFile ();
	}

}
