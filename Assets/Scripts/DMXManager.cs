﻿/*
 * Creare 8 GAMEOBJECT per le luci e dargli un tag LEDSTRIPES a tutti i Quad creati in EDITOR
 */
using UnityEngine;
using System;
using System.Collections;
using ArtNet;
using UnityEngine.UI;


public class DMXManager : MonoBehaviour 
{
	public int DMXChannels = 26;
	public  int NumMaxLedStripes = 8;
	public byte[] DMXData;
	public Color32[] ColorsLEDtoDMX;
	public GameObject[] LedStripes;
	public float speedPulse;
	public float passo;

	private byte colorA;
	private float aValue;

	// ART NET Object
	public static ArtNet.Engine ArtEngine; 

	public float sound;

	public enum SetDmxMode { Off, WhiteSound, RedLoading, RedPulse, Pulsing, Ascending, Discending, Editor, FastRnd, KinectLeds, Black, BluePulse, WhitePulse, RedSound, OffAndSetRedSoundMedusa };
	public static SetDmxMode actualMode;
	public float pulseSpeed = 400.0f;

	public int[] indexRnd;

	void Start () 
	{
		indexRnd = new int[NumMaxLedStripes];
		for (int i = 0; i < indexRnd.Length; i++) 
		{
			indexRnd [i] = UnityEngine.Random.Range (0, 640*480);
		}
		speedPulse = 1;

		// SET DMX SOUND REACTIVE MODE WHITE
		//actualMode = DMXManager.SetDmxMode.WhiteSound;

		DMXData = new byte[DMXChannels]; // CHANNEL MAX del decoder DMX == 25
		ColorsLEDtoDMX = new Color32[NumMaxLedStripes]; // COLORS LED STRIPES
		LedStripes = new GameObject[NumMaxLedStripes]; // LedStripes in Editor Unity
		LedStripes = GameObject.FindGameObjectsWithTag ("LEDSTRIPES"); // cerco tutti i GameObjects con il TAG LEDSTRIPES e riempio l'array di GO
		//istanzio il gestore della comunicazione:
		ArtEngine = new ArtNet.Engine("Open DMX Ethernet", "255.255.255.255"); // IP
		//apro il canale di comunicazione con i parametri inseriti sopra:
		ArtEngine.Start ();

		for (int i = 0; i< DMXData.Length; i++) 
		{	
			//set all light to black on quit application
			byte[] setBlack = new byte[DMXChannels];
			ArtEngine.SendDMX (0, setBlack, setBlack.Length);
		}

		//WriteLogSendEmail.WriteLogFile ("DMX Start " + "DMXChannels: " + DMXChannels + "LED STRIPES Connected: " + NumMaxLedStripes);
	}

	// Update is called once per frame
	void Update () 
	{	
		// Pick Up Loudness SoundReactive
		sound = GetComponent<SoundReactive> ().loudness;

		// DRAW IN EDITOR PREPARING LED COLORS 
		for (int i = 0; i < LedStripes.Length; i++) 
		{
			switch (actualMode) 
				{
					case SetDmxMode.Off:
						//print ("Led Turn OFF");
						LedStripes [i].GetComponent<Renderer> ().material.color = Color.black;
						ColorsLEDtoDMX [i] = LedStripes [i].GetComponent<Renderer> ().material.color;	
						break;

					case SetDmxMode.OffAndSetRedSoundMedusa:
						//print ("Led Turn OFF");
						LedStripes [i].GetComponent<Renderer> ().material.color = Color.black;
						ColorsLEDtoDMX [i] = LedStripes [i].GetComponent<Renderer> ().material.color;	
						actualMode = SetDmxMode.RedSound;
						break;

					case SetDmxMode.WhiteSound:
						//print ("DMX Sound Reactive");
						speedPulse = 0.15f;
						passo = 28.7f;
						aValue = Mathf.Abs (Mathf.Sin (Time.time * speedPulse * sound + (i * passo))) * 255;
						colorA = (byte)aValue;
						ColorsLEDtoDMX [i] = new Color32 (colorA, colorA, colorA, colorA);
						
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.RedSound:
						//print ("DMX Sound Reactive");
						speedPulse = 0.15f;
						passo = 28.7f;
						aValue = Mathf.Abs( Mathf.Sin (Time.time * speedPulse * sound + (i * passo))) * 255;
						colorA = (byte)aValue;
						ColorsLEDtoDMX [i] = new Color32 (colorA, 0 ,0, 255);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.RedLoading:
						// RED PULSE
						speedPulse = 1.0f;
						passo = 1.0f;
						aValue = Mathf.Abs( Mathf.Sin (Time.time * speedPulse * sound + (i * passo))) * 255;
						colorA = (byte)aValue;
						ColorsLEDtoDMX [i] = new Color32 (colorA, 0 ,0, colorA);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.Black:
						aValue = Mathf.Lerp (aValue, 0, Time.time * speedPulse);
						ColorsLEDtoDMX [i] = new Color32 ((byte)colorA, 255 , 255, 255);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.Pulsing:
						aValue = Mathf.Abs( Mathf.Sin (Time.time * speedPulse )) * 255;
						colorA = (byte)aValue;
						ColorsLEDtoDMX [i] = new Color32 (colorA, colorA ,colorA, colorA);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.Ascending:
						
						//PASSAGGIO ASCENDENTE
						aValue = Mathf.Abs( Mathf.Sin (Time.time * speedPulse +  (i * passo) )) * 220;
						colorA = (byte)aValue;
						ColorsLEDtoDMX [i] = new Color32 (colorA, colorA ,colorA, colorA);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.Discending:
						//PASSAGGIO DISCENDENTE
						aValue = Mathf.Abs( Mathf.Sin (Time.time * (-speedPulse) +  (i * passo) )) * 255;
						colorA = (byte)(aValue);
						ColorsLEDtoDMX [i] = new Color32 (colorA, colorA ,colorA, colorA);
			
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.Editor:
						print ("Led Editor Colors");
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;
						
					case SetDmxMode.FastRnd:
						print ("Led Fast Random FX");
						bool rnd = System.Convert.ToBoolean (UnityEngine.Random.Range (0, 2));
						Color32 rndCol;
						if (rnd == false)
							rndCol = Color.black;
						else { rndCol = Color.white; }
				        
						LedStripes [i].GetComponent<Renderer> ().material.color = rndCol;
						ColorsLEDtoDMX[i] = LedStripes [i].GetComponent<Renderer> ().material.color;
						break;
						

					case SetDmxMode.KinectLeds:
						// GET RANDOM PIXELS FROM DEPTHMAP KINECT 
						
						ColorsLEDtoDMX [i] = Color.Lerp (Color.red, Color.blue, PointCloud.pix [indexRnd [i]].r);
						ColorsLEDtoDMX[i] =  Color.Lerp (Color.black, ColorsLEDtoDMX [i], Mathf.Abs(Mathf.Sin(Time.time)));

						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];

					break;
				
					case SetDmxMode.RedPulse:
						// blcack to red
						float r = Mathf.PingPong (Time.time * pulseSpeed, 255);
						ColorsLEDtoDMX [i] = new Color32 ((byte)r, 0 ,0, 255);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.BluePulse:
						
						float b = Mathf.Lerp (0, 255, Mathf.Abs(Mathf.Sin(Time.time)));
						ColorsLEDtoDMX [i] = new Color32 (0, 0 ,(byte)b, 255);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;

					case SetDmxMode.WhitePulse:

						float w = Mathf.Lerp (0, 255, Mathf.Abs(Mathf.Sin(Time.time)));
						ColorsLEDtoDMX [i] = new Color32 ((byte)w, (byte)w ,(byte)w, 255);
						LedStripes [i].GetComponent<Renderer> ().material.color = ColorsLEDtoDMX [i];
						break;


					default:
						break;
				}

		}
		// Preparo il pacchetto DMX riempito con i colori in Editor
		for ( int i = 0; i< DMXData.Length; i++)
		{
			DMXData = Color32toByte.Color32ArrayToByteArray (ColorsLEDtoDMX);
		}

		if (actualMode == SetDmxMode.WhiteSound) 
		{
			DMXData [26] = 255;
		} 
		else 
		{
			DMXData [26] = 0;
		}

	

		// Send To DMX
		ArtEngine.SendDMX (0, DMXData, DMXData.Length);
		
		//print ("LED: " + actualMode);
	}



#region LEDS BEHIAVIOUR

	public void LedTurnOff()
	{
		byte[] setBlack = new byte[DMXData.Length];
		for (int i = 0; i< DMXData.Length; i++) 
		{	
			//set all light to black on quit application
			setBlack [i] = 0;
		}

		ArtEngine.SendDMX (0, setBlack, setBlack.Length);
	}

#endregion

	void OnApplicationQuit ( )
	{	
		LedTurnOff ();
		//print ("quit application");
		Debug.LogError ("quit application");
	}
			
}
