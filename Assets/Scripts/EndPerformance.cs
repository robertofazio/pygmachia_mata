﻿using UnityEngine;
using System.Collections;
using freenect;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EndPerformance : MonoBehaviour 
{
	public GameObject panelEnd;
	public Text finalPoints;
	private float timer;

	void Start () 
	{
		timer = 0.0f;

		Soffitto._soffitto = false;
		Pavimento._pavimento = false;
		Soffitto.speed = 0.7f;
		Pavimento.speed = 0.6f;

		AudioManager.PygmachiaMainAudioSource.clip = Resources.Load("Audio/Mata-SoundTrack/MaTa-SoundTrack-BackGround-5") as AudioClip; 
		AudioManager.PygmachiaMainAudioSource.Play ();

		Destroy (GameObject.Find ("GAME MANAGER").GetComponent<SpawObjects> ());
		GameObject.Find ("Panel-Points").SetActive (false);

		DMXManager.actualMode = DMXManager.SetDmxMode.Off;

		finalPoints.text = DestroyCollider.cnt.ToString ();

		SaveScore.Get_ID_SaveFile ();
	}
	
	void Update ()
	{
		if (AudioManager.PygmachiaMainAudioSource.clip.name == "MaTa-SoundTrack-BackGround-5" && AudioManager.PygmachiaMainAudioSource.isPlaying == false) 
		
		{
//			UnityKinect.kinect.LED.Color = freenect.LEDColor.BlinkGreen;
//			UnityKinect.kinect.Close ();
//			Kinect.Shutdown ();

			Destroy (GameObject.Find ("GAME MANAGER").gameObject);
			Destroy (GameObject.Find ("Main Camera").GetComponent<MovCamera> ());
			//Destroy (GameObject.Find ("AUDIO"));
			//Destroy (GameObject.Find ("VIDEOLOGO"));
			//Destroy (GameObject.Find ("DMX"));
			//Destroy (GameObject.Find ("MEDUSASTUFF"));		

			GameManager._actualGame = GameManager.Statement.Idle;

			SceneManager.LoadScene ("Start");

		} 
		else 
		{
			panelEnd.SetActive (true);

		}
	}
	
}
