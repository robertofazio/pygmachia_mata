﻿using UnityEngine;
using System.Collections;

public class SpawObjects : MonoBehaviour 
{
	//public GameObject[] spawnObjects;
	private int numMax = 5;
	public Transform bornTargetLeft;
	public Transform bornTargetRight;
	public Transform bornTargetCenter;
	private Vector3 emissionPos;
	public float thrust;

	public static bool triggerStartSpawin = false;
	private float waitTime = 2.0f;
	private float timerCenter;
	private float spawingTimeFreq = 10.0f;
	public GameObject myPrefab;
	public GameObject myPrefabCenter;

	void Start () 
	{
		UnityEngine.Physics.gravity = new Vector3 (0f, -.2f, 0f);


		//spawnObjects = new GameObject[numMax];

//		for (int i = 0; i < spawnObjects.Length; i++) 
//		{
//			spawnObjects [i] = Resources.Load ("Prefabs/oggetto" + i, typeof(GameObject)) as GameObject;
//			if (spawnObjects [i].GetComponent<OggettoBehiavor> () == null)
//				spawnObjects [i].AddComponent<OggettoBehiavor> ();
//
//			if (spawnObjects [i].GetComponent<DeathObjects> () == null)
//				spawnObjects [i].AddComponent<DeathObjects> ();
//		}

		timerCenter = 0.0f;
	}
	
	void Update () 
	{
		if (triggerStartSpawin) 
		{
			timerCenter = 0;
			reStart ();
			triggerStartSpawin = false;

		}
//
//		if(Input.GetKeyDown(KeyCode.O))
//			triggerStartSpawin = true;

		timerCenter += Time.deltaTime;

		//CrashDetect.SetExePoint ("test");

	}

	public void reStart ()
	{
		StartCoroutine (StartSpawing (waitTime));

	}

	public IEnumerator StartSpawing(float _waitTime)
	{
		bool rndLeftRight = System.Convert.ToBoolean(Random.Range (0, 2));
		bool rndCenter = false;

		if (timerCenter > spawingTimeFreq) 
		{
			rndCenter = true;
			timerCenter = 0;
		}
			

		if (rndLeftRight && !rndCenter)
			emissionPos = bornTargetLeft.transform.position;
		if (!rndLeftRight && !rndCenter)
			emissionPos = bornTargetRight.transform.position;
		
		Quaternion rndRot = Quaternion.Euler (new Vector3 (Random.Range(0,360), Random.Range(0,360), Random.Range(0,360)));
		GameObject _instance = Instantiate (myPrefab , emissionPos, rndRot ) as GameObject;
		_instance.GetComponent<Rigidbody> ().AddForce (Vector3.back * thrust);
		_instance.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range(0,2), Random.Range(0,6), Random.Range(0,4)));

		if (rndCenter) 
		{
			rndRot = Quaternion.Euler (new Vector3 (0,0,0));
			GameObject _CenterIstance = Instantiate (myPrefabCenter, bornTargetCenter.transform.position, rndRot ) as GameObject;
			_CenterIstance.GetComponent<Rigidbody> ().AddForce (Vector3.back * thrust);
			//_CenterIstance.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range(0,2), Random.Range(0,6), Random.Range(0,4)));
		}

		yield return new WaitForSeconds(waitTime);

		reStart ();

	}
}
