﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TriggerPhoto : MonoBehaviour 
{
	public static bool switchCamera;
	public static int cnt;
	public static StreamWriter streamFile;
	public static int numMaxCazzotti = 12;

	void Start () 
	{
		cnt = 0;
		switchCamera = System.Convert.ToBoolean(Random.Range (0, 2));

		//Debug.LogError (switchCamera);

		if(File.Exists("TakePhoto.txt"))
			File.Delete("TakePhoto.txt");
	}
	
	void Update () 
	{
		// scena kinect
		if (switchCamera) 
		{
			if ((Trigger.triggerRight || Trigger.triggerLeft) && GameManager.kinectStuffActive)
				cnt += 1;
		}

		// scena medusa
		else if (!switchCamera) 
		{
			if ((Medusa.punchRight || MedusaSx.punchLeft) && GameManager._actualGame == GameManager.Statement.Medusa)
				cnt += 1;
		}

		if (cnt == numMaxCazzotti) 
		{
			if (File.Exists ("ScoreSaved.txt"))
				File.Delete ("ScoreSaved.txt");
			
			SetTriggerPhoto ();
		}

	}

	// Salvo il File nell'indirizzo dell'applicazione UnityWebGL
	public static void SetTriggerPhoto()
	{
		cnt += 1;

		if (File.Exists ("TakePhoto.txt")) 
		{
			File.Delete ("TakePhoto.txt");
		}

		streamFile = new StreamWriter("TakePhoto.txt");
		streamFile.Write("*");
		streamFile.Close();
	//	Debug.LogError("Trigger Photo File Saved ");
	}
}
