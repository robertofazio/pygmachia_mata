﻿using UnityEngine;
using System.Collections;

public class Soffitto : MonoBehaviour 
{
	public Vector3 desireScale;
	public static float speed = 0.2f;

	public static bool _soffitto = false;

	void Start () 
	{
		desireScale = new Vector3 (0, 0, 60);
		this.transform.localScale = desireScale;
	}
	
	void Update () 
	{
		if (_soffitto)
			desireScale = new Vector3 (60, 0, 60);
		else if(!_soffitto)
			desireScale = new Vector3 (0, 0, 60);

		this.transform.localScale = Vector3.Lerp (this.transform.localScale, desireScale, Time.deltaTime * speed);
	}
}
