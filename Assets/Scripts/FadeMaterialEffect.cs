﻿using UnityEngine;
using System.Collections;

public class FadeMaterialEffect : MonoBehaviour 
{
	public Material fadeMat;
	public static Color32[] fadeCols;
	public Material fadeMatSX;
	public  Color32[] fadeColsSX;

	public static float aValue;

	public static bool checkRight;
	public static bool checkLeft;

	public bool right, left;
	public float speed = 1.2f;
	public static float v;
	public static float vSx;

	public float minimum = 0.0F;
	public float maximum =  255.0F;

	public float minimumSx = 0.0F;
	public float maximumSx =  255.0F;

	// starting value for the Lerp    
	public static float t = 0.0f;
	public static float tSx = 0.0f;

	void Start () 
	{
		fadeCols = new Color32[Medusa.numSeg];
		fadeColsSX = new Color32[MedusaSx.numSeg];

		checkRight = false;
		checkLeft = false;
		//SetStartBlack ();
		right = false;
		left = false;

		SetStartBlack ();
	
	}
	
	void Update () 
	{
		FadingLeft ();
		FadingRight ();
	}


	void FadingRight()
	{
		for (int i = 0; i < Medusa.numSeg; i++) 
		{
			fadeCols [i] = fadeMat.color;
			fadeCols [i].a = (byte)v;
			Medusa.segments [Medusa.numSeg - 1 - i].GetComponent<MeshRenderer> ().material.color = fadeCols [i];
		}

		if (checkRight)
			right = true;

		if (right == true) 
		{
			maximum = 255;
			Medusa.segments [1].GetComponent<BoxCollider> ().enabled = true;

		}
		else if (right == false)
		{
			maximum = 0; 
		}

		t += speed * Time.deltaTime;

		v = Mathf.Lerp(minimum, maximum, t);

		if (t > 1.0f)
		{
			float temp = maximum;
			maximum = minimum;
			minimum = temp;
			t = 0.0f;
			right = false;
			checkRight = false;

			Medusa.segments [1].GetComponent<BoxCollider> ().enabled = false;

		}

	}

	void FadingLeft()
	{
		for (int i = 0; i < MedusaSx.numSeg; i++) 
		{
			fadeColsSX [i] = fadeMatSX.color;
			fadeColsSX [i].a = (byte)vSx;
			MedusaSx.segments [MedusaSx.numSeg - 1 - i].GetComponent<MeshRenderer> ().material.color = fadeColsSX [i];
		}

		if (checkLeft)
			left = true;

		if (left == true) 
		{
			maximumSx = 255;
			MedusaSx.segments [1].GetComponent<BoxCollider> ().enabled = true;
		}
		else if (left == false)
		{
			maximumSx = 0; 
		}

		tSx += speed * Time.deltaTime;
		vSx = Mathf.Lerp(minimumSx, maximumSx, tSx);
		if (tSx > 1.0f)
		{
			float temp = maximumSx;
			maximumSx = minimumSx;
			minimumSx = temp;
			tSx = 0.0f;
			left = false;
			checkLeft = false;
			MedusaSx.segments [1].GetComponent<BoxCollider> ().enabled = false;
		}

	}
	public static void SetStartBlack()
	{
		for (int i = 0; i < MedusaSx.numSeg; i++) 
		{
			aValue = 0;
			fadeCols [i].a = (byte)aValue;
			MedusaSx.segments [i].GetComponent<MeshRenderer> ().material.color = fadeCols [i];
			Medusa.segments [i].GetComponent<MeshRenderer> ().material.color = fadeCols [i];

		}
	}


}
