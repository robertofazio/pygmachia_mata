﻿using UnityEngine;
using System.Collections;

public class DeathObjects : MonoBehaviour 
{

	void Update () 
	{
		if (this.transform.localPosition.z < -30)
			Destroy (this.gameObject);
		
		else if (AudioManager.PygmachiaMainAudioSource.clip.name == "MaTa-SoundTrack-BackGround-5") 
		{
			Destroy (this.gameObject);
		}
	}
}
