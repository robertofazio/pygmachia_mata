﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovCamera : MonoBehaviour {

	public PointCloud pc;
	public Camera myCam;

	private Vector3 desirePos;	
	private Quaternion desireRot;
	private Vector3 actualRot;

	private float Speed = 6.0f;
	//public static bool enableMovment;

	public enum MovimentiCamera { Front, LeftPunch, RightPunch, MedusaLeftPunch, MedusaRightPunch, MedusaShield };
	public static MovimentiCamera movimenti;

	public void StartMovment (MovimentiCamera _movimenti)
	{
		switch (_movimenti) 
		{

		case MovimentiCamera.Front:
			IdlePositionCamera ();
			movimenti = _movimenti;

			AudioManager.BackgroundVolume = 0.5f;

			break;

		case MovimentiCamera.LeftPunch:
			LeftPunchCamera ();
			movimenti = _movimenti;
			break;

		case MovimentiCamera.RightPunch:
			RightPunchCamera ();
			movimenti = _movimenti;
			break;

		case MovimentiCamera.MedusaLeftPunch:
			MedusaLeftPunchCamera ();
			movimenti = _movimenti;
			break;

		case MovimentiCamera.MedusaRightPunch:
			MedusaRightPunchCamera ();
			movimenti = _movimenti;
			break;

		case MovimentiCamera.MedusaShield:
			MedusaShieldCamera ();
			movimenti = _movimenti;
			break;

		

		default:
			//print("Default");
			break;
		}

		//return movimenti;
	}

	void Start()
	{
		desirePos = new Vector3 (0, 0, -10);
		actualRot = new Vector3 (0, 0, 0);
		myCam.transform.position = desirePos;
		myCam.transform.rotation = Quaternion.Euler(actualRot);
	}

	void IdlePositionCamera()
	{
		desirePos = new Vector3 (0, 0, -10);
		desireRot = Quaternion.Euler(new Vector3 (0, 0, 0));
	} 

	void LeftPunchCamera()
	{
		desirePos = new Vector3 (200, 0, 200);
		desireRot = Quaternion.Euler(new Vector3 (0f, -30f, 0f));
	}
		
	void RightPunchCamera()
	{
		desirePos = new Vector3 (-200, 0, 200);
		desireRot = Quaternion.Euler(new Vector3 (0f, 30f, 0f));
	}
	// MEDUSA CAM ROT
	void MedusaLeftPunchCamera()
	{
		desirePos = new Vector3 (2, 0, -9);
		desireRot = Quaternion.Euler(new Vector3 (0f, -10f, 0f));
	}

	void MedusaRightPunchCamera()
	{
		desirePos = new Vector3 (-2, 0, -9);
		desireRot = Quaternion.Euler(new Vector3 (0f, 10, 0f));
	}

	void MedusaShieldCamera()
	{
		desirePos = new Vector3 (0, 0, -10);
		desireRot = Quaternion.Euler(new Vector3 (0, 0, 0));

		//HACK hard set fadeout oggetti
		FadeMaterialEffect.t = 1.01f;
		FadeMaterialEffect.tSx = 1.01f;
	}



	void Update () 
	{
		myCam.transform.localPosition = Vector3.Lerp (myCam.transform.localPosition, desirePos, Time.deltaTime * Speed);
		myCam.transform.rotation = Quaternion.Lerp (myCam.transform.rotation, desireRot, Time.deltaTime * Speed);

		if(Trigger.triggerLeft == true)
		{
			StartMovment(MovimentiCamera.LeftPunch);
			AudioManager.LoadPlayAudioColpo ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Colpo", false, true);
			AudioManager.BackgroundVolume = 0.1f;

			Trigger.triggerLeft = false;
			PointCloud.boolInstantiateLeft = true;

		}

		if(Trigger.triggerRight == true)
		{
			StartMovment (MovimentiCamera.RightPunch);
			AudioManager.LoadPlayAudioColpo ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Colpo", false, true);
			AudioManager.BackgroundVolume = 0.1f;

			Trigger.triggerRight = false;
			PointCloud.boolInstantiateRight = true;

		}
			
		if (myCam.transform.position.x >= 195  || myCam.transform.position.x <= -195) // Z desirePos
		{
			StartMovment (MovimentiCamera.Front);
		} 
			
		if (GameManager._actualGame == GameManager.Statement.Medusa) 
		{
			MedusaBehiavor ();
			//Debug.LogError (GameManager._actualGame);
		}
			

	}

	public void MedusaBehiavor()
	{
		// MEDUSA PUNCH LEFT & RIGHT
		if(MedusaSx.punchLeft == true && Shield.v == 0)
		{
			StartMovment (MovimentiCamera.MedusaLeftPunch);
			AudioManager.LoadPlayAudioColpo ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Colpo-Medusa", false, true);
			//AudioManager.PygmachiaMainAudioSource.volume = 1;
			MedusaSx.punchLeft = false;
			//Debug.LogError ("left");
			FadeMaterialEffect.checkLeft = true;
			FadeMaterialEffect.aValue = 0;

		}

		if(Medusa.punchRight == true  && Shield.v == 0)
		{
			StartMovment (MovimentiCamera.MedusaRightPunch);
			AudioManager.LoadPlayAudioColpo ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Colpo-Medusa", false, true);
			//AudioManager.PygmachiaMainAudioSource.volume = 1;
			Medusa.punchRight = false;

			FadeMaterialEffect.checkRight = true;
			FadeMaterialEffect.aValue = 0;

			//Debug.LogError ("right");
		}

		if (myCam.transform.position.x >= 1.95f  || myCam.transform.position.x <= -1.95f) // Z desirePos
		{
			StartMovment (MovimentiCamera.Front);
			//Debug.LogError ("pos: >=15 && <=15");
		}
	}
}
